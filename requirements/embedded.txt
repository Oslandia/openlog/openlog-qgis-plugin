# Embedded requirements
#
# Python packages required by the plugin and which are not included into the embedded Python in QGIS (mainly Windows).
#
# Typical command to install:
# python -m pip install --no-deps -U -r requirements/embedded.txt -t openlog/embedded_external_libs
# -----------------------
chevron
sqlalchemy>=1.4,<2.0
GeoAlchemy2>=0.13.3,<0.14
pyqtgraph>=0.13.3
xplordb==0.13.8
pint>=0.24.4
pluggy
# pint dependencies
flexcache
flexparser
typing_extensions
platformdirs

py-machineid
# extensions dep
mplstereonet
