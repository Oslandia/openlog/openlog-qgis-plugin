from datetime import datetime

import numpy as np
import pytest

from openlog.datamodel.assay.generic_assay import (
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
    GenericAssay,
)
from openlog.datamodel.assay.uncertainty import AssayColumnUncertainty
from openlog.datamodel.connection.interfaces.assay_interface import AssayInterface
from openlog.datamodel.connection.openlog_connection import OpenLogConnection

# ---------------- TESTS FOR ASSAY TABLE CREATION ----------------------------------
from tests.qgis.tools import (
    create_assay_discrete_definition_object,
    create_assay_extended_definition_object,
)


def test_assay_table_creation(test_connection: OpenLogConnection):
    """Test  connection assay table creation

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """

    # Spatialite connection can add assay table
    assert test_connection.get_assay_iface().can_import_assay()

    (definition, table) = create_assay_discrete_definition_object(
        "assay_table_creation",
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    table.schema = test_connection.get_assay_iface().default_assay_schema()

    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    # New assay is available in assay definition
    assert test_connection.get_assay_iface().get_all_available_assay_definitions() == [
        definition
    ]

    assert (
        test_connection.get_assay_iface().get_assay_table_definition(
            "assay_table_creation"
        )
        == table
    )


def test_table_creation_rollback(test_connection):
    """Test  connection allow table creation rollback

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """

    (definition, table) = create_assay_discrete_definition_object(
        "assay_table_creation",
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.rollback()
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    # New assay is available in assay definition
    assert test_connection.get_assay_iface().get_all_available_assay_definitions() == [
        definition
    ]

    assert (
        test_connection.get_assay_iface().get_assay_table_definition(
            "assay_table_creation"
        )
        == table
    )


def test_existing_assay_creation(test_connection):
    """Test  connection exception raise if same assay variable inserted twice

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    (definition, table) = create_assay_discrete_definition_object(
        "assay_1",
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)

    (new_definition, new_table) = create_assay_discrete_definition_object(
        "assay_2",
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    new_definition.variable = definition.variable

    # An import exception is raised when same assay is inserted twice
    with pytest.raises(AssayInterface.ImportException):
        test_connection.get_assay_iface().add_assay_table(definition, table)

    assert (
        len(test_connection.get_assay_iface().get_all_available_assay_definitions())
        == 0
    )


def test_existing_table_creation(test_connection):
    """Test  connection exception raise if same assay table inserted twice

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    (definition, table) = create_assay_discrete_definition_object(
        "assay_1",
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)

    (new_definition, new_table) = create_assay_discrete_definition_object(
        "assay_2",
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    new_table.table_name = table.table_name

    # An import exception is raised when same assay is inserted twice
    with pytest.raises(AssayInterface.ImportException):
        test_connection.get_assay_iface().add_assay_table(new_definition, new_table)

    assert (
        len(test_connection.get_assay_iface().get_all_available_assay_definitions())
        == 0
    )


def test_assay_not_available_read(test_connection):
    """Test  connection exception raise if assay not available

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    # An read exception is raised when the assay is not available
    assay_name = "invalid_assay"
    expected_message = f"Assay {assay_name} not available."
    with pytest.raises(AssayInterface.ReadException) as expect:
        test_connection.get_assay_iface().get_assay("test", assay_name)

    assert expected_message in str(expect.value)


def import_assay_data(
    test_connection: OpenLogConnection,
    assay: GenericAssay,
    x_value: [],
    y_values: {str: []},
):
    assay.import_x_values = x_value
    assay.import_y_values = y_values

    test_connection.get_assay_iface().import_assay_data([assay])
    test_connection.commit()


# ---------------- TESTS FOR ASSAY INSERTION : INVALID INPUT ARRAY ----------------------------------
def test_invalid_array_insertion(test_connection):
    assay_name = "assay_discrete"
    hole_id = "test"
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)

    # Invalid dimension
    expected_message = (
        "Invalid discrete x import data, numpy array should have one dimension"
    )
    with pytest.raises(AssayInterface.ImportException) as expect:
        import_assay_data(
            test_connection,
            assay,
            [(0, 1), (1, 0), (2, 3), (3, 4), (4, 5)],
            {"y": [0, 1, 2, 3, 4]},
        )
    assert expected_message in str(expect.value)

    assay_name = "assay_extended"
    hole_id = "test"
    (definition, table) = create_assay_extended_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)

    # Invalid dimension
    expected_message = (
        "Invalid extended x import data, numpy array should have 2 dimensions"
    )
    with pytest.raises(AssayInterface.ImportException) as expect:
        import_assay_data(test_connection, assay, [0, 1, 2, 3], {"y": [0, 1, 2, 3]})
    assert expected_message in str(expect.value)

    # Invalid column
    expected_message = "Invalid downhole data, column 'invalid' not available in downhole data definition"
    with pytest.raises(AssayInterface.ImportException) as expect:
        import_assay_data(
            test_connection,
            assay,
            [(0, 1), (1, 0), (2, 3), (3, 4), (4, 5)],
            {"invalid": [0, 1, 2, 3]},
        )
    assert expected_message in str(expect.value)


# ---------------- TESTS FOR ASSAY INSERTION : DEPTH ----------------------------------
def test_depth_assay_insertion(test_connection):
    """Test  connection for depth assay insertion

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    assay_name = "assay"
    hole_id = "test"
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    import_assay_data(test_connection, assay, [0, 1, 2, 3], {"y": [0, 1, 2, 3]})


def test_invalid_depth_assay_insertion(test_connection):
    """Test  connection for invalid depth assay insertion

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    assay_name = "assay"
    hole_id = "test"
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    # Invalid data type : str
    with pytest.raises(OpenLogConnection.ImportException):
        import_assay_data(
            test_connection, assay, ["a", "b", "c", "d"], {"y": [0, 1, 2, 3]}
        )

    # Invalid data type : datetime
    with pytest.raises(OpenLogConnection.ImportException):
        import_assay_data(
            test_connection,
            assay,
            [
                datetime(2022, 3, 29, 9, 0, 0),
                datetime(2022, 3, 29, 10, 0, 0),
                datetime(2022, 3, 29, 11, 0, 0),
                datetime(2022, 3, 29, 12, 0, 0),
            ],
            {"y": [0, 1, 2, 3]},
        )

    # Invalid array size
    expected_message = (
        "Invalid downhole data, x and y array for column 'y' doesn't have same size"
    )
    with pytest.raises(AssayInterface.ImportException) as expect:
        import_assay_data(test_connection, assay, [0, 1, 2, 3, 4], {"y": [0, 1, 2, 3]})
    assert expected_message in str(expect.value)


# ---------------- TESTS FOR ASSAY INSERTION : TIME ----------------------------------
def test_time_assay_creation(test_connection):
    """Test  connection for time assay insertion

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    assay_name = "assay"
    hole_id = "test"
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.TIME,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    import_assay_data(
        test_connection,
        assay,
        [
            datetime(2022, 3, 29, 9, 0, 0),
            datetime(2022, 3, 29, 10, 0, 0),
            datetime(2022, 3, 29, 11, 0, 0),
        ],
        {"y": [0, 1, 2]},
    )


def test_invalid_time_assay_creation(test_connection):
    """Test  connection for invalid time assay insertion

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    assay_name = "assay"
    hole_id = "test"
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.TIME,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    # Invalid data type : str
    with pytest.raises(OpenLogConnection.ImportException):
        import_assay_data(
            test_connection, assay, ["a", "b", "c", "d"], {"y": [0, 1, 2, 3]}
        )

    # Invalid data type : float
    with pytest.raises(OpenLogConnection.ImportException):
        import_assay_data(
            test_connection,
            assay,
            [
                1.0,
                2.0,
                3.0,
                4.0,
            ],
            {"y": [0, 1, 2, 3]},
        )

    # Invalid array size
    expected_message = (
        "Invalid downhole data, x and y array for column 'y' doesn't have same size"
    )
    with pytest.raises(AssayInterface.ImportException) as expect:
        import_assay_data(
            test_connection,
            assay,
            [datetime(2022, 3, 29, 9, 0, 0), datetime(2022, 3, 29, 10, 0, 0)],
            {"y": [0]},
        )
    assert expected_message in str(expect.value)


def check_available_assay(
    test_connection: OpenLogConnection,
    expected_definition: AssayDefinition,
    assay_name: str,
    hole_id: str,
    expected_x_values: [],
    expected_y_values: [],
):
    """
    Check for available assay

    Args:
        test_connection: OpenLogConnection
        expected_definition: AssayDefinition that should be available in database
        assay_name: assay name for assay data check
        hole_id: hole id for assay data check
        expected_x_values: expected assay data x values
        expected_y_values: expected assay data y values
    """

    # Check that added assay are available
    assert (
        len(test_connection.get_assay_iface().get_all_available_assay_definitions())
        == 1
    )
    assert (
        test_connection.get_assay_iface().get_all_available_assay_definitions()[0]
        == expected_definition
    )

    # Check assay data
    read_assay = test_connection.get_assay_iface().get_assay(hole_id, assay_name)
    array_x, array_y = read_assay.get_all_values("y")
    np.testing.assert_array_equal(array_x, np.array(expected_x_values))
    np.testing.assert_array_equal(array_y, np.array(expected_y_values))


# ---------------- TESTS FOR ASSAY READ : DEPTH ----------------------------------
def test_discrete_depth_assay_read(test_connection):
    """Test read of discrete depth assay

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    assay_name = "assay"
    hole_id = "test"
    second_hole_id = "test2"

    # Create assay
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    # Insert data for 2 holes
    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    assay.import_x_values = [0, 1, 2, 3]
    assay.import_y_values = {"y": [4, 5, 6, 7]}

    second_assay = GenericAssay(hole_id=second_hole_id, assay_definition=definition)
    second_assay.import_x_values = [8, 9, 10, 11]
    second_assay.import_y_values = {"y": [12, 13, 14, 15]}

    test_connection.get_assay_iface().import_assay_data([assay, second_assay])
    test_connection.commit()

    read_assay = test_connection.get_assay_iface().get_assay(hole_id, assay_name)
    read_second_assay = test_connection.get_assay_iface().get_assay(
        second_hole_id, assay_name
    )

    # Test correct values from hole_id definition
    array_x, array_y = read_assay.get_all_values("y")
    np.testing.assert_array_equal(array_x, np.array(assay.import_x_values))
    np.testing.assert_array_equal(array_y, np.array(assay.import_y_values["y"]))

    second_array_x, second_array_y = read_second_assay.get_all_values("y")
    np.testing.assert_array_equal(
        second_array_x, np.array(second_assay.import_x_values)
    )
    np.testing.assert_array_equal(
        second_array_y, np.array(second_assay.import_y_values["y"])
    )

    # Test values from interval (only for one assay)
    array_x, array_y = read_assay.get_values_from_depth("y", 1, 2)
    np.testing.assert_array_equal(array_x, np.array([1, 2]))
    np.testing.assert_array_equal(array_y, np.array([5, 6]))

    array_x, array_y = read_assay.get_values_from_depth("y", 10, 11)
    assert array_x.size == 0
    assert array_y.size == 0


def test_extended_depth_assay_read(test_connection):
    """Test read of extended depth assay

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    hole_id = "test"
    assay_name = "assay"
    (definition, table) = create_assay_extended_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    x_values = [(-1, 0), (0, 1), (1, 2), (2, 3), (3, 4)]
    y_values = {"y": [3, 4, 5, 6, 7]}

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    import_assay_data(test_connection, assay, x_values, y_values)

    read_assay = test_connection.get_assay_iface().get_assay(hole_id, assay_name)
    array_x, array_y = read_assay.get_all_values("y")

    np.testing.assert_array_equal(array_x, np.array(x_values))
    np.testing.assert_array_equal(array_y, np.array(y_values["y"]))

    # Inside current extent
    array_x, array_y = read_assay.get_values_from_depth("y", 1, 2)
    np.testing.assert_array_equal(array_x, np.array([(0, 1), (1, 2), (2, 3)]))
    np.testing.assert_array_equal(array_y, np.array([4, 5, 6]))

    # Inside only one data
    array_x, array_y = read_assay.get_values_from_depth("y", 1.5, 1.9)
    np.testing.assert_array_equal(array_x, np.array([(1, 2)]))
    np.testing.assert_array_equal(array_y, np.array([5]))

    # Not inside
    array_x, array_y = read_assay.get_values_from_depth("y", -10, -5)
    np.testing.assert_array_equal(array_x, np.array([]))
    np.testing.assert_array_equal(array_y, np.array([]))

    # Inside right
    array_x, array_y = read_assay.get_values_from_depth("y", 2.5, 5)
    np.testing.assert_array_equal(array_x, np.array([(2, 3), (3, 4)]))
    np.testing.assert_array_equal(array_y, np.array([6, 7]))

    # Inside left
    array_x, array_y = read_assay.get_values_from_depth("y", -10, 0.5)
    np.testing.assert_array_equal(array_x, np.array([(-1, 0), (0, 1)]))
    np.testing.assert_array_equal(array_y, np.array([3, 4]))


def test_assay_depth_insert(openlog_plugin, test_connection):
    """Test assay content

    Args:
        openlog_plugin: OpenlogPlugin
        test_connection: fixture to get a configured OpenLogConnection
    """
    hole_id = "test"
    assay_name = "assay"

    # Create assay table and insert some data
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    x_values = [0, 1, 2, 3]
    y_values = {"y": [0, 1, 2, 3]}

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    import_assay_data(test_connection, assay, x_values, y_values)

    # Check assay
    check_available_assay(
        test_connection,
        definition,
        assay_name,
        hole_id,
        x_values,
        y_values["y"],
    )


# ---------------- TESTS FOR ASSAY READ : TIME ----------------------------------
def test_discrete_time_assay_read(test_connection):
    """Test read of discrete time assay

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    hole_id = "test"
    second_hole_id = "test2"
    assay_name = "assay"

    # Create assay table
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.TIME,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    # Insert data for 2 holes
    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    assay.import_x_values = [
        datetime(2022, 3, 29, 9, 0, 0),
        datetime(2022, 3, 29, 10, 0, 0),
        datetime(2022, 3, 29, 11, 0, 0),
    ]
    assay.import_y_values = {"y": [0, 1, 2]}

    second_assay = GenericAssay(hole_id=second_hole_id, assay_definition=definition)
    second_assay.import_x_values = [
        datetime(2021, 3, 29, 9, 0, 0),
        datetime(2021, 3, 29, 10, 0, 0),
        datetime(2021, 3, 29, 11, 0, 0),
    ]
    second_assay.import_y_values = {"y": [3, 4, 5]}

    test_connection.get_assay_iface().import_assay_data([assay, second_assay])
    test_connection.commit()

    read_assay = test_connection.get_assay_iface().get_assay(hole_id, assay_name)
    read_second_assay = test_connection.get_assay_iface().get_assay(
        second_hole_id, assay_name
    )

    # Test correct values from hole_id definition
    array_x, array_y = read_assay.get_all_values("y")
    np.testing.assert_array_equal(array_x, np.array(assay.import_x_values))
    np.testing.assert_array_equal(array_y, np.array(assay.import_y_values["y"]))

    second_array_x, second_array_y = read_second_assay.get_all_values("y")
    np.testing.assert_array_equal(
        second_array_x, np.array(second_assay.import_x_values)
    )
    np.testing.assert_array_equal(
        second_array_y, np.array(second_assay.import_y_values["y"])
    )

    # Test values from interval (only for one assay)
    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(2022, 3, 29, 9, 30, 0), datetime(2022, 3, 29, 10, 30, 0)
    )
    np.testing.assert_array_equal(array_x, np.array([datetime(2022, 3, 29, 10, 0, 0)]))
    np.testing.assert_array_equal(array_y, np.array([1]))

    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(2021, 3, 29, 9, 30, 0), datetime(2021, 3, 29, 10, 30, 0)
    )
    assert array_x.size == 0
    assert array_y.size == 0


def test_extended_time_assay_read(test_connection):
    """Test read of extended time assay

    Args:
        test_connection: fixture to get a configured OpenLogConnection
    """
    hole_id = "test"
    assay_name = "assay"
    (definition, table) = create_assay_extended_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.TIME,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    y = 2022
    m = 3
    x_values = [
        (datetime(y, m, 1), datetime(y, m, 2)),
        (datetime(y, m, 2), datetime(y, m, 3)),
        (datetime(y, m, 3), datetime(y, m, 4)),
        (datetime(y, m, 4), datetime(y, m, 5)),
        (datetime(y, m, 5), datetime(y, m, 6)),
    ]
    y_values = {"y": [3, 4, 5, 6, 7]}

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    import_assay_data(test_connection, assay, x_values, y_values)

    read_assay = test_connection.get_assay_iface().get_assay(hole_id, assay_name)
    array_x, array_y = read_assay.get_all_values("y")

    np.testing.assert_array_equal(array_x, np.array(x_values))
    np.testing.assert_array_equal(array_y, np.array(y_values["y"]))

    # Inside current extent
    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(y, m, 3), datetime(y, m, 4)
    )
    np.testing.assert_array_equal(
        array_x,
        np.array(
            [
                (datetime(y, m, 2), datetime(y, m, 3)),
                (datetime(y, m, 3), datetime(y, m, 4)),
                (datetime(y, m, 4), datetime(y, m, 5)),
            ]
        ),
    )
    np.testing.assert_array_equal(array_y, np.array([4, 5, 6]))

    # Inside only one data
    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(y, m, 3, 9), datetime(y, m, 3, 10)
    )
    np.testing.assert_array_equal(
        array_x, np.array([(datetime(y, m, 3), datetime(y, m, 4))])
    )
    np.testing.assert_array_equal(array_y, np.array([5]))

    # Not inside
    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(y, m - 2, 1), datetime(y, m - 1, 1)
    )
    np.testing.assert_array_equal(array_x, np.array([]))
    np.testing.assert_array_equal(array_y, np.array([]))

    # Inside right
    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(y, m, 4, 10), datetime(y, m, 10)
    )
    np.testing.assert_array_equal(
        array_x,
        np.array(
            [
                (datetime(y, m, 4), datetime(y, m, 5)),
                (datetime(y, m, 5), datetime(y, m, 6)),
            ]
        ),
    )
    np.testing.assert_array_equal(array_y, np.array([6, 7]))

    # Inside left
    array_x, array_y = read_assay.get_values_from_time(
        "y", datetime(y, m - 1, 4, 10), datetime(y, m, 2, 10)
    )
    np.testing.assert_array_equal(
        array_x,
        np.array(
            [
                (datetime(y, m, 1), datetime(y, m, 2)),
                (datetime(y, m, 2), datetime(y, m, 3)),
            ]
        ),
    )
    np.testing.assert_array_equal(array_y, np.array([3, 4]))


def test_assay_time_insert(openlog_plugin, test_connection):
    """Test assay content

    Args:
        openlog_plugin: OpenlogPlugin
        test_connection: fixture to get a configured OpenLogConnection
    """
    hole_id = "test"
    assay_name = "assay"

    # Create assay table and insert some data
    (definition, table) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition, table)
    test_connection.commit()

    x_values = [0, 1, 2, 3]
    y_values = {"y": [0, 1, 2, 3]}

    assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    import_assay_data(test_connection, assay, x_values, y_values)

    # Check assay
    check_available_assay(
        test_connection,
        definition,
        assay_name,
        hole_id,
        x_values,
        y_values["y"],
    )


# ---------------- TESTS FOR MULTIPLE DOMAIN USE  ----------------------------------
def test_multiple_assay_creation(test_connection):
    depth_assay = "depth"
    time_assay = "time"
    hole_id = "test"

    (definition_depth, table_depth) = create_assay_discrete_definition_object(
        depth_assay,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition_depth, table_depth)

    (definition_time, table_time) = create_assay_discrete_definition_object(
        time_assay,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.TIME,
        AssaySeriesType.NUMERICAL,
    )
    test_connection.get_assay_iface().add_assay_table(definition_time, table_time)

    test_connection.commit()

    depth_assay = GenericAssay(hole_id=hole_id, assay_definition=definition_depth)
    depth_assay.import_x_values = [0, 1, 2, 3]
    depth_assay.import_y_values = {"y": [0, 1, 2, 3]}

    time_assay = GenericAssay(hole_id=hole_id, assay_definition=definition_time)
    time_assay.import_x_values = [
        datetime(2022, 3, 29, 9, 30, 0),
        datetime(2022, 3, 29, 10, 30, 0),
    ]
    time_assay.import_y_values = {"y": [0, 1]}

    test_connection.get_assay_iface().import_assay_data([depth_assay, time_assay])
    test_connection.commit()


# ---------------- TESTS FOR ASSAY WITH UNCERTAINTY  ----------------------------------
def test_uncertainty_assay_creation(test_connection):
    assay_name = "depth"
    hole_id = "test"

    (definition, table_depth) = create_assay_discrete_definition_object(
        assay_name,
        test_connection.get_assay_iface().default_assay_schema(),
        AssayDomainType.DEPTH,
        AssaySeriesType.NUMERICAL,
    )
    uncertainty = AssayColumnUncertainty(upper_whisker_column="uncertainty")
    definition.columns["y"].uncertainty = uncertainty
    test_connection.get_assay_iface().add_assay_table(definition, table_depth)

    test_connection.commit()

    y_values = {"y": [0, 1, 2, 3], "uncertainty": [0.5, 0.5, 0.5, 0.5]}
    x_values = [0, 1, 2, 3]

    depth_assay = GenericAssay(hole_id=hole_id, assay_definition=definition)
    depth_assay.import_x_values = x_values
    depth_assay.import_y_values = y_values
    test_connection.get_assay_iface().import_assay_data([depth_assay])
    test_connection.commit()

    read_assay = test_connection.get_assay_iface().get_assay(hole_id, assay_name)
    array_x, array_y = read_assay.get_all_values("y")

    np.testing.assert_array_equal(array_x, np.array(x_values))
    np.testing.assert_array_equal(array_y, np.array(y_values["y"]))

    array_x, array_y = read_assay.get_all_values("uncertainty")
    np.testing.assert_array_equal(array_x, np.array(x_values))
    np.testing.assert_array_equal(array_y, np.array(y_values["uncertainty"]))
