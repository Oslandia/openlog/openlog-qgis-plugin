import pytest
from PyQt5 import QtCore
from PyQt5.QtWidgets import QInputDialog, QMessageBox
from qgis.core import QgsCoordinateReferenceSystem

from openlog.datamodel.connection.openlog_connection import OpenLogConnection
from openlog.gui.import_data.database_import import DatabaseImportWizard
from tests.qgis.tools import create_file

PERSON_WIZARD_INDEX = 0
DATASET_WIZARD_INDEX = 1
COLLAR_WIZARD_INDEX = 2
SURVEY_WIZARD_INDEX = 3
LITHOLOGY_WIZARD_INDEX = 4


@pytest.fixture()
def database_wizard_with_connection(qtbot, test_connection: OpenLogConnection):
    """
    Fixture to create a DatabaseImportWizard with defined xplordb connection

    Args:
        test_connection: OpenLogConnection
        qtbot: qtbot fixture
    """
    widget = DatabaseImportWizard(test_connection)
    qtbot.addWidget(widget)
    yield widget
    test_connection.rollback()


@pytest.fixture()
def database_wizard_with_person(database_wizard_with_connection, mocker):
    """
    Fixture to create a database wizard with a created person

    Args:
        database_wizard_with_connection: fixture to create a DatabaseImportWizard
        litedb_no_data: fixture to create a lite xplordb database
        mocker: mocker for QInputDialog for person creation
    """

    # mock person creation
    mocker.patch.object(QInputDialog, "getText", return_value=("xdb", True))
    database_wizard_with_connection.data_import_wizards[
        PERSON_WIZARD_INDEX
    ].person_selection._create_person()
    yield database_wizard_with_connection


@pytest.fixture()
def database_wizard_with_dataset(database_wizard_with_person, mocker):
    """
    Fixture to create a database wizard with a created dataset

    Args:
        database_wizard_with_person: fixture to create a DatabaseImportWizard with a person creation
        mocker: mocker for QInputDialog for dataset creation
    """
    # Force initialization to get used person
    database_wizard_with_person.data_import_wizards[
        DATASET_WIZARD_INDEX
    ].initializePage()
    # mock dataset creation
    mocker.patch.object(QInputDialog, "getText", return_value=("dataset", True))
    database_wizard_with_person.data_import_wizards[
        DATASET_WIZARD_INDEX
    ].dataset_selection._create_dataset()
    yield database_wizard_with_person


@pytest.fixture()
def database_wizard_with_collar_no_crs(database_wizard_with_dataset, tmp_path):
    """
    Fixture to create a database wizard with collar page without crs selection

    Args:
        database_wizard_with_dataset: fixture to create a DatabaseImportWizard with a dataset/person creation
        tmp_path: pytest temporary path for collar csv creation
    """
    assert database_wizard_with_dataset.data_import_wizards[
        DATASET_WIZARD_INDEX
    ].validatePage()
    collar_wizard = database_wizard_with_dataset.data_import_wizards[
        COLLAR_WIZARD_INDEX
    ]
    csv_file = create_file(
        tmp_path,
        "collar.csv",
        [
            collar_wizard.HOLE_ID_COL,
            collar_wizard.X_COL,
            collar_wizard.Y_COL,
            collar_wizard.Z_COL,
        ],
        [["collar", "0", "0", "0"]],
        ";",
    )
    collar_wizard.dataset_edit.set_delimiter(";")
    collar_wizard.dataset_edit.filename_edit.lineEdit().setText(str(csv_file))
    collar_wizard.dataset_edit._update_table_and_fields()

    yield database_wizard_with_dataset


@pytest.fixture()
def database_wizard_with_collar_with_crs(database_wizard_with_collar_no_crs):
    """
    Fixture to create a database wizard with collar page with crs selection

    Args:
        database_wizard_with_collar_no_crs: fixture to create a DatabaseImportWizard with collar page without crs selection
    """
    collar_wizard = database_wizard_with_collar_no_crs.data_import_wizards[
        COLLAR_WIZARD_INDEX
    ]
    collar_wizard.dataset_edit.set_crs(QgsCoordinateReferenceSystem("EPSG:3857"))
    yield database_wizard_with_collar_no_crs


def database_wizard_with_survey_data(
    database_wizard_with_collar_with_crs, tmp_path, survey_data: [[str]]
):
    survey_wizard = database_wizard_with_collar_with_crs.data_import_wizards[
        SURVEY_WIZARD_INDEX
    ]
    csv_file = create_file(
        tmp_path,
        "survey.csv",
        [
            survey_wizard.HOLE_ID_COL,
            survey_wizard.DIP_COL,
            survey_wizard.AZIMUTH_COL,
            survey_wizard.LENGTH_COL,
        ],
        survey_data,
        ";",
    )
    survey_wizard.dataset_edit.set_delimiter(";")
    survey_wizard.dataset_edit.filename_edit.lineEdit().setText(str(csv_file))
    survey_wizard.dataset_edit._update_table_and_fields()
    return database_wizard_with_collar_with_crs


@pytest.fixture()
def database_wizard_with_valid_survey_data(
    database_wizard_with_collar_with_crs, tmp_path
):
    """
    Fixture to create a database wizard with survey page

    Args:
        database_wizard_with_collar_with_crs: fixture to create a database wizard with collar page with crs selection
        tmp_path: pytest temporary path for survey csv creation
    """
    survey_data = [
        ["collar", "0", "0", "0"],
        ["collar", "0", "0", "10"],
        ["collar", "0", "0", "50"],
    ]
    yield database_wizard_with_survey_data(
        database_wizard_with_collar_with_crs, tmp_path, survey_data
    )


@pytest.fixture()
def database_wizard_with_invalid_survey_data(
    database_wizard_with_collar_with_crs, tmp_path
):
    """
    Fixture to create a database wizard with invalid survey page

    Args:
        database_wizard_with_collar_with_crs: fixture to create a database wizard with collar page with crs selection
        tmp_path: pytest temporary path for survey csv creation
    """

    survey_data = [
        ["collar", "0", "0", "0"],
        ["collar", "0", "0", "0"],
        ["collar", "0", "0", "50"],
    ]
    yield database_wizard_with_survey_data(
        database_wizard_with_collar_with_crs, tmp_path, survey_data
    )


def _database_wizard_with_lithology_data(database_wizard, tmp_path, values: [[str]]):
    lithology_wizard = database_wizard.data_import_wizards[LITHOLOGY_WIZARD_INDEX]
    csv_file = create_file(
        tmp_path,
        "lith.csv",
        [
            lithology_wizard.HOLE_ID_COL,
            lithology_wizard.LITH_CODE_COL,
            lithology_wizard.FROM_COL,
            lithology_wizard.TO_COL,
        ],
        values,
        ";",
    )
    lithology_wizard.dataset_edit.set_delimiter(";")
    lithology_wizard.dataset_edit.filename_edit.lineEdit().setText(str(csv_file))
    lithology_wizard.dataset_edit._update_table_and_fields()
    return database_wizard


@pytest.fixture()
def database_wizard_with_lithology_data(database_wizard_with_collar_with_crs, tmp_path):
    """
    Fixture to create a database wizard with lithology page

    Args:
        database_wizard_with_collar_with_crs: fixture to create a database wizard with collar page with crs selection
        tmp_path: pytest temporary path for lithology csv creation
    """
    values = [
        ["collar", "lith_code", "0", "10"],
        ["collar", "lith_code", "10", "100"],
        ["collar", "lith_code", "100", "500"],
    ]
    yield _database_wizard_with_lithology_data(
        database_wizard_with_collar_with_crs, tmp_path, values
    )


@pytest.fixture()
def database_wizard_with_gap_overlap_lithology_data(
    database_wizard_with_collar_with_crs, tmp_path
):
    """
    Fixture to create a database wizard with lithology page with gap and overlap data

    Args:
        database_wizard_with_collar_with_crs: fixture to create a database wizard with collar page with crs selection
        tmp_path: pytest temporary path for lithology csv creation
    """
    values = [
        ["collar", "lith_code", "0", "10"],
        ["collar", "lith_code", "10", "100"],
        ["collar", "lith_code", "150", "200"],
        ["collar", "lith_code", "175", "250"],
        ["collar", "lith_code", "250", "500"],
    ]
    yield _database_wizard_with_lithology_data(
        database_wizard_with_collar_with_crs, tmp_path, values
    )


def test_no_person_defined(database_wizard_with_connection):
    """
    Test person selection is not validated if no person selected

    Args:
        database_wizard_with_connection: fixture to create a DatabaseImportWizard
        litedb_no_data: fixture to create a lite xplordb database
    """

    # person selection not validated (no person available)
    assert not database_wizard_with_connection.data_import_wizards[
        PERSON_WIZARD_INDEX
    ].validatePage()


@pytest.fixture()
def database_wizard_with_person(database_wizard_with_connection, mocker):
    """
    Fixture to create a database wizard with a created person

    Args:
        database_wizard_with_connection: fixture to create a DatabaseImportWizard
        mocker: mocker for QInputDialog for person creation
    """

    # mock person creation
    input_mock = mocker.patch.object(
        QInputDialog, "getText", return_value=("xdb", True)
    )
    database_wizard_with_connection.data_import_wizards[
        PERSON_WIZARD_INDEX
    ].person_selection._create_person()
    input_mock.assert_called_once()
    yield database_wizard_with_connection


def test_person_creation(database_wizard_with_person):
    """
    Test person selection page is validated if a person is selected

    Args:
        database_wizard_with_person:
    """
    # person selection is validated
    assert database_wizard_with_person.data_import_wizards[
        PERSON_WIZARD_INDEX
    ].validatePage()


def test_no_dataset_defined(database_wizard_with_person):
    """
    Test person dataset is not validated if no dataset selected

    Args:
        database_wizard_with_person: fixture to create a DatabaseImportWizard with a person creation
    """
    assert database_wizard_with_person.data_import_wizards[
        PERSON_WIZARD_INDEX
    ].validatePage()

    # dataset selection not validated (no dataset available)
    assert not database_wizard_with_person.data_import_wizards[
        DATASET_WIZARD_INDEX
    ].validatePage()


def test_dataset_creation(database_wizard_with_dataset):
    """
    Test dataset selection page is validated if a dataset is selected

    Args:
        database_wizard_with_dataset: fixture to create a DatabaseImportWizard with a dataset/person creation
    """
    # dataset selection is validated
    assert database_wizard_with_dataset.data_import_wizards[
        DATASET_WIZARD_INDEX
    ].validatePage()


def test_collar_no_crs(database_wizard_with_collar_no_crs, mocker):
    """
    Test collar page is not validated without crs definition

    Args:
        database_wizard_with_collar_no_crs: fixture to create a DatabaseImportWizard with a collar data definition
        mocker: pytest-mock to accept QMessageBox warning
    """
    collar_wizard = database_wizard_with_collar_no_crs.data_import_wizards[
        COLLAR_WIZARD_INDEX
    ]
    # mock no crs selected
    msg_box_ok_mock = mocker.patch.object(
        QMessageBox, "warning", return_value=QMessageBox.Ok
    )
    assert not collar_wizard.validatePage()
    msg_box_ok_mock.assert_called_once()


def test_collar_with_import(database_wizard_with_collar_with_crs):
    """
    Test collar page is validated if data and crs are selected

    Args:
        database_wizard_with_collar_with_crs: fixture to create a database wizard with collar page with crs selection
    """
    collar_wizard = database_wizard_with_collar_with_crs.data_import_wizards[
        COLLAR_WIZARD_INDEX
    ]
    assert collar_wizard.validatePage()


def test_survey_validate(database_wizard_with_valid_survey_data):
    """
    Test survey page is always validated

    Args:
        database_wizard_with_valid_survey_data: fixture to create database wizard with survey page
    """
    survey_wizard = database_wizard_with_valid_survey_data.data_import_wizards[
        SURVEY_WIZARD_INDEX
    ]
    assert survey_wizard.validatePage()


def test_invalid_survey_data_import(database_wizard_with_invalid_survey_data, mocker):
    """
    Test no error occurs when valid data is imported

    Args:
        database_wizard_with_invalid_survey_data: fixture to create database wizard with invalid survey page
    """
    confirmation_page = database_wizard_with_invalid_survey_data.confirmation_page
    msg_box_mock = mocker.patch.object(
        QMessageBox, "warning", return_value=QMessageBox.Ok
    )
    assert not confirmation_page.validatePage()
    msg_box_mock.assert_called_once()
