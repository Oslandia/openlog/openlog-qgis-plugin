import pytest
from psycopg2 import connect
from PyQt5.QtWidgets import QFileDialog
from pytest_qgis import MockMessageBar, QgisInterface
from xplordb.import_ddl import ImportDDL
from xplordb.schema import Schema

from openlog.datamodel.connection.openlog_connection import (
    Connection,
    OpenLogConnection,
)
from openlog.datamodel.connection.spatialite.spatialite_connection import (
    SpatialiteConnection,
)
from openlog.datamodel.connection.xplordb.xplordb_connection import XplordbConnection
from openlog.plugin_main import OpenlogPlugin


class layerTreeViewClass:
    def setCurrentLayer(self, layer):
        pass


class OpenLogQgisInterface(QgisInterface):
    def registerOptionsWidgetFactory(self, factory):
        pass

    def unregisterOptionsWidgetFactory(self, factory):
        pass

    def removeDockWidget(self, widget):
        pass

    def showAttributeTable(self, layer):
        pass

    def layerTreeView(self):
        return layerTreeViewClass()


@pytest.fixture(scope="session")
def openlog_qgis_iface(qgis_canvas, qgis_parent) -> OpenLogQgisInterface:
    return OpenLogQgisInterface(qgis_canvas, MockMessageBar(), qgis_parent)


def pytest_addoption(parser):
    parser.addoption("--host", action="store", default="localhost")
    parser.addoption("--user", action="store", default="postgres")
    parser.addoption("--password", action="store")
    parser.addoption("--port", action="store", default=5432)
    parser.addoption("--db", action="store")
    parser.addoption("--connection_type", action="store", default="spatialite")


@pytest.fixture(scope="module")
def host(request):
    return request.config.getoption("--host")


@pytest.fixture(scope="module")
def user(request):
    return request.config.getoption("--user")


@pytest.fixture(scope="module")
def password(request):
    return request.config.getoption("--password")


@pytest.fixture(scope="module")
def db(request):
    return request.config.getoption("--db")


@pytest.fixture(scope="module")
def port(request):
    return request.config.getoption("--port")


@pytest.fixture(scope="module")
def connection_type(request):
    return request.config.getoption("--connection_type")


@pytest.fixture()
def connection(host: str, port: int, db: str, user: str, password: str) -> Connection:
    return Connection(host=host, port=port, database=db, user=user, password=password)


@pytest.fixture()
def db_session(host: str, port: int, db: str, user: str, password: str):
    """
    Create a pyscog2 connection with specified parameters
    parameters are defined from pytest call option.

    For example pytest --host localhost --port 5432 --db xplordb_test --user postgres --password postgres

    (default  values are defined in conftest.py)

    :param host: host used for connection
    :param port: port used for connection
    :param db: database name used for connection
    :param user:user used for connection
    :param password:password used for connection
    """
    session = connect(database=db, host=host, port=port, user=user, password=password)
    yield session
    session.close()


@pytest.fixture()
def clean_db(db_session):
    """
    pytest fixture to clear any xplordb schema from a database

    Args:
        db_session:
    """
    for schema in Schema.db_schema_list():
        _drop_schema(db_session, schema.name)


def _drop_schema(session, schema: str):
    with session.cursor() as cur:
        cur.execute(f"DROP SCHEMA IF EXISTS {schema} CASCADE")
        cur.close()
    session.commit()


@pytest.fixture()
def litedb_no_data(host, user, password, db, port):
    """
    pytest fixture to create a xplordb database with lite schema without sample data
    """
    _import_db(host, user, password, db, port, False, False)


def _import_db(
    host: str,
    user: str,
    password: str,
    db: str,
    port: int,
    import_data: bool,
    full_db: bool,
):
    import_ddl_ = ImportDDL(host=host, user=user, password=password, db=db, port=port)
    import_ddl_.import_xplordb_schema(import_data=import_data, full_db=full_db)


@pytest.fixture()
def openlog_plugin(openlog_qgis_iface: OpenLogQgisInterface) -> OpenlogPlugin:
    """
    pytest fixture to initialize OpenlogPlugin

    Args:
        openlog_qgis_iface: fixture to simulate QGisInterface
    """
    plugin = OpenlogPlugin(openlog_qgis_iface)
    # avoid splash during testing
    plugin.splash_active = False
    plugin._check_openlog_version = lambda: None
    plugin.initGui()
    yield plugin
    plugin.unload()


@pytest.fixture
def plugin_spatialite_create(
    openlog_plugin: OpenlogPlugin, tmp_path, mocker
) -> OpenlogPlugin:
    """
    pytest fixture to initialize a spatialite connection

    Args:
        openlog_plugin: OpenlogPlugin
        tmp_path: pytest temp path for spatialite db creation
        mocker: pytest-mock to simulate QFileDialog return value
    """
    # mock connection save file name
    mocker.patch.object(
        QFileDialog, "getSaveFileName", return_value=(tmp_path / "test.db", True)
    )
    openlog_plugin._spatialite_create()
    yield openlog_plugin


@pytest.fixture()
def spatialite_connection(
    plugin_spatialite_create: OpenlogPlugin,
) -> SpatialiteConnection:
    """
    pytest fixture to get configured spatialite connection

    Args:
        plugin_spatialite_create: OpenlogPlugin initialized with a spatialite connection

    Returns: SpatialiteConnection

    """
    return plugin_spatialite_create.openlog_connection


@pytest.fixture()
def test_connection(
    spatialite_connection, connection, connection_type
) -> OpenLogConnection:
    if connection_type == "spatialite":
        yield spatialite_connection
        spatialite_connection.rollback()
    elif connection_type == "xplordb":
        import_ddl_ = ImportDDL(
            host=connection.host,
            user=connection.user,
            password=connection.password,
            db=connection.database,
            port=int(connection.port),
        )
        import_ddl_.import_xplordb_schema(import_data=False, full_db=False)
        xplordb_connection = XplordbConnection(connection=connection)
        yield xplordb_connection
        xplordb_connection.rollback()
        session = connect(
            database=connection.database,
            host=connection.host,
            port=connection.port,
            user=connection.user,
            password=connection.password,
        )
        for schema in Schema.db_schema_list():
            _drop_schema(session, schema.name)
        session.close()
