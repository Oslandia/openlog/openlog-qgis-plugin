import pytest

from openlog.gui.utils.check_versions import (
    get_latest_qgis_version,
    get_openlog_last_version,
)


def test_qgis_release_scrap():
    exception = False
    try:
        result = get_latest_qgis_version()
    except Exception:
        exception = True
    assert exception is False


def test_openlog_release_scrap():
    exception = False
    try:
        result = get_openlog_last_version()
    except Exception:
        exception = True
    assert exception is False
