import numpy as np

from openlog.core.affine_transform import affine_transform, make_affine


def test_no_transformation():
    src = np.random.rand(10, 3)
    dst = src
    expected_affine_param = np.array(
        [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_x_translation():
    a0 = 2.3

    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x + a0
    dst[:, 1] = y
    dst[:, 2] = z

    expected_affine_param = np.array(
        [a0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_x_scale():
    a1 = 2.3

    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x * a1
    dst[:, 1] = y
    dst[:, 2] = z

    expected_affine_param = np.array(
        [0.0, a1, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_x_y_transform():
    a2 = 2.3

    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x + a2 * y
    dst[:, 1] = y
    dst[:, 2] = z

    expected_affine_param = np.array(
        [0.0, 1.0, a2, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_y_translation():
    b0 = 2.3
    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x
    dst[:, 1] = y + b0
    dst[:, 2] = z

    expected_affine_param = np.array(
        [0.0, 1.0, 0.0, 0.0, b0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_y_scale():
    b2 = 2.3

    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x
    dst[:, 1] = y * b2
    dst[:, 2] = z

    expected_affine_param = np.array(
        [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, b2, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_y_x_transform():
    b1 = 2.3

    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x
    dst[:, 1] = y + b1 * x
    dst[:, 2] = z

    expected_affine_param = np.array(
        [0.0, 1.0, 0.0, 0.0, 0.0, b1, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_z_translation():
    c0 = 2.3
    src = np.random.rand(10, 3)
    x = src[:, 0]
    y = src[:, 1]
    z = src[:, 2]
    dst = np.zeros(src.shape)
    dst[:, 0] = x
    dst[:, 1] = y
    dst[:, 2] = z + c0

    expected_affine_param = np.array(
        [0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, c0, 0.0, 0.0, 1.0]
    )
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)


def test_complex_transformation():
    expected_affine_param = np.random.rand(3 * 4)
    src = np.random.rand(10, 3)

    dst = affine_transform(src, expected_affine_param)
    params, _ = make_affine(src, dst)
    np.testing.assert_almost_equal(expected_affine_param, params)
