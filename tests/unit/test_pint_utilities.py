import numpy as np

from openlog.core import pint_utilities


def test_invalid_unit():
    assert not pint_utilities.is_pint_unit("")
    assert not pint_utilities.is_pint_unit("uyiyui")


def test_valid_unit():
    assert pint_utilities.is_pint_unit("pounds")
    assert pint_utilities.is_pint_unit("B/kg")
    assert pint_utilities.is_pint_unit("ms")
    assert pint_utilities.is_pint_unit("m/s")
    # Since pint utility version 0.21.0 % or percent is a valid unit : see https://github.com/hgrecco/pint/issues/1277
    assert pint_utilities.is_pint_unit("%kg")


def test_possible_conversion():
    assert not pint_utilities.can_convert("m/s", "")
    assert not pint_utilities.can_convert("", "m/s")
    assert pint_utilities.can_convert("m/s", "km/s")
    assert not pint_utilities.can_convert("m/s", "kg/s")
    assert not pint_utilities.can_convert("fsdfdsfsd", "fsdfdsfds")


def test_conversion_to_base_unit():
    val = np.array([1, 10, 100, 1000])

    # Base unit is kg
    res = pint_utilities.convert_to_base_unit("g", val)
    np.testing.assert_equal(res, val / 1000)

    # Base unit is m
    res = pint_utilities.convert_to_base_unit("km", val)
    np.testing.assert_equal(res, val * 1000)

    # Invalid unit
    res = pint_utilities.convert_to_base_unit("dsqdsqdq", val)
    np.testing.assert_equal(res, val)


def test_conversion_from_base_unit():
    val = np.array([1, 10, 100, 1000])

    # Base unit is kg
    res = pint_utilities.convert_from_base_unit("g", val)
    np.testing.assert_equal(res, val * 1000)

    # Base unit is m
    res = pint_utilities.convert_from_base_unit("km", val)
    np.testing.assert_equal(res, val / 1000)

    # Invalid unit
    res = pint_utilities.convert_from_base_unit("fdsfds", val)
    np.testing.assert_equal(res, val)


def test_from_to_conversion():
    val = np.array([1, 10, 100, 1000])

    res = pint_utilities.unit_conversion("g", "mg", val)
    np.testing.assert_equal(res, val * 1000)

    res = pint_utilities.unit_conversion("km/s", "m/ms", val)
    np.testing.assert_equal(res, val)

    res = pint_utilities.unit_conversion("dm/s", "m/s", val)
    np.testing.assert_equal(res, val / 10.0)

    # Invalid unit
    res = pint_utilities.unit_conversion("fdsfds/s", "fdsfsds", val)
    np.testing.assert_equal(res, val)
