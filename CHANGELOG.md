# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

- Update BDGEO station symbologies

### Removed

-->
## 1.4.1 - 2025-03-11

Fixes:

- technicals logs : Add default symbology for undefined crepine
- technicals logs : remove scale factor to use data in meters and angle in ° from input data
- extended/discrete switch causing QGIS crash when selecting another collar

## 1.4.0 - 2025-01-30

Updated version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  
**Important:** This new version come with new `xplordb` release (0.13.7). See [changelog](https://gitlab.com/geolandia/openlog/xplordb/-/blob/develop/CHANGELOG.md?ref_type=heads).  
[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  

Features:  

- premium : sort drillholes by geographic orientation #148
- premium : add depth ticks marks on traces #184
- Plot titles can now be dynamically truncated #228
- ability to import detection limits data #251 #372
- premium : detection limits visualization #251
- use of an alias table for assay/column names flexibility #345 #372
- ability to modify collar attributes through a dedicated widget #346
- planned traces : support for dedicated coordinates x,y,z #347 #372
- ability to save/load symbology in database or a file #364 #372
- ability to save/load openlog state #364
- add of planned coordinates in collar creation widget #375
- openlog premium : introduction of a minimal openlog version for compatibility #389


Fixes:  

- bug when visualizing extended categorical data with gaps #365
- use unit when projecting numerical data on traces #379
- bug when saving/loading categorical symbology (svg patterns) #382
- various bug when loading QGIS project with openlog #386
- refactoring : use SRID for all spatial operations (split, desurveying, inspector) #390

For developpers:  

- new test testing each plot item creation + altitude switch #387
- use of geometries stored in original SRID for TraceSplitter, GeoExtractor, inspector (label + projection), depth ticks, assay views #393


## 1.3.0 - 2024-12-12

Updated version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  
**Important:** This new version come with new `xplordb` release (0.13.5). See [changelog](https://gitlab.com/geolandia/openlog/xplordb/-/blob/develop/CHANGELOG.md?ref_type=heads).  
[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  

Features:  

- display plot configuration through contextual menu #298
- support for external plugin installation (openlog premium) #325 #360
- development of inspector_projection premium extension #307
- development of manage_assay premium extension #308
- display all assay views as QGIS layers #313 #332 #334 #336
- ability to import assay from a QGIS layer #314
- add color ramps preview #333
- add extra columns for an assay table : dataset, person, import date #335
- remove Stacked checkbox in plot configuration #341
- add option to set header row in import wizard #349 !403
- put documentation source in a dedicated repository #356
- block import if defined data is invalid #359 !403
- filter through .csv and .txt extensions when importing data !403  
- add metadata.txt section to store integrated extensions #325  
- support for BDGeo trace geometries #380
- DiagramsMaker is now stored in PlotWidgetContainer #385
- widgets resizing is done by taking account of diagrams #385
- creation of BoldBoolparameter for color ramp and diagrams #385


Fixes:  

- implementation of st_3dlinesubstring and st_3dinterpolatepoint for spatialite and postgis #351
- Modified unit not taken into account for stacked figure #319
- fix bug when mapping uncertainty columns #331  
- spatialite collar layer error after database creation #350
- fix bug about inspector line and extended numerical assays #357
- force refreshing collar attribute table after collar import !405  
- force showing add and remove buttons in collar import wizard #361
- update dependencies versions according to QGIS embedded python version #374 #373
- fix infinite loop in PlotWidgetContainer #381

## 1.2.2 - 2024-07-17  

Fix version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  


Fixes:  

- spherical data : azimuth is dip direction #337
- spherical data : for planes, dip should be inside 0 90 range #337
- spherical data : use dedicated symbol for reversed polarity #337

## 1.2.1 - 2024-07-05  

Fix version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  


Fixes:  

- bug to display nominal assay #327  
- stratigraphy displaying for BDGeo #321  
- downhole data importer (spherical mapping) !378
- AssayVisualizationConfigTreeModel.is_splittable method !378  
- imagery range calculation !378  
- config creation for extended structural assays !378
- numerical discrete/extended switch with no trace geometry !378  


## 1.2.0 - 2024-07-02  

Updated version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  
**Important:** This new version come with new `xplordb` release (0.13.4). See [changelog](https://gitlab.com/geolandia/openlog/xplordb/-/blob/develop/CHANGELOG.md?ref_type=heads).  
[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:  

- ability to import collar metadata (optional columns) #84  
- display collar metadata in collar attribute table #309  
- propose default name for new categories #303  
- ability to import structural data (spherical or polar) #305
- representation of structural assay in logviewer using USGS standards #306 #315
- new options for setting collar elevation #312  
- BDGeo : support for technical logs representation #323


Bug fixes:  

- select/unselect collar become unresponsive if logviewer is closed #297  
- inspector label is draggable #304  
- windows : move .pyd sqlalchemy files during OpenLog upgrade/uninstallation #311  
- fix QGIS crash related to inspector !366


For developers:  

- implementation of a new (extensible) plot container #316  
- refactoring : use of plugins via `pluggy` package (connection and data type) #318  
- ci : testing integration of external plugins !374

## 1.1.0 - 2024-03-29

Updated version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  
**Important:** This new version come with new `xplordb` release (0.13.2). See [changelog](https://gitlab.com/geolandia/openlog/xplordb/-/blob/develop/CHANGELOG.md?ref_type=heads).  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:  

- minimap implementation for context serie visualization !349  
- ability to reorder plots by drag'n'drop with right click !347
- ability to reorder plots according to collar X or Y coordinates !347
- full support with new xplordb schema !350
- full support of planned trace with associated symbology !350
- ability to switch plots from drilled length to altitude for both effective/planned trace !273
- display of measure's geographical coordinates un inspector line tool #279
- create a global "Plot options" parameter in stacked configuration #301
- minimap support for stacked plot !360
- add new icons to buttons #302


Fixes:

- handle properly table/column name in postgis queries #296
- fix hierarchical order in Downhole data tree model : assay -> column -> collar #294
- title of a user-defined QGIS project and layers symbology are preserved #291
- fix broken link to splash background image on Windows #290
- add a survey at EOH when desurveying to create correct geometries #288
- fix a bug when merging contiguous categories #287
- shrink Y axis title if text is truncated #260
- disable add/remove button at survey import #300


## 1.0.1 - 2024-02-05  

Fix version for the first stable version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  

New features:  

- add a button to subscribe to newsletter in settings menu #285  

Fixes:  

- fix date-related format at collar import #280  
- newly created database table are dropped if assay import fail #281  
- fix plugin crash on QGIS exit #282  
- table and columns can have SQL keywords as name #283  
- fix help button redirection link #284  
- update/add metadata fields (dependencies, donation)  

## 1.0.0 - 2024-01-23  

First stable version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:  

- Geotic database integration for displaying assays #261  
- UI simplification by removing specific lithology support #263 #264  
- Gap/overlap solver is now applied to generic extended data !326  
- Gap/overlap solver options are inside combobox #265  
- Remove "Mouse mode" in pyqtgraph contextual options #270  
- Splash screen for newsletter inscription

Fixes:  

- Acquire database connection removed from UI #266  
- Fix plugin crash due to 0 thickness extended data #267  
- Contiguous categories are merged in logviewer #268  


For developpers:  

- New unit tests about contiguous category items #271  
- Update of unit test about assay interpolation #271  
- Remove lithology unit tests !326  
- Update python version in CI yaml !326  
- Update qgis docker image version used in CI yaml !316  


## 0.13.0 - 2023-12-05
Thirteenth alpha version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:  

- Log-viewer support for discrete categorical assays and its interactive legend !304 !309  
- Cross-section symbology for extended categorical assays #154  
- Cross-section symbology for discrete numerical assays #155  
- Cross-section symbology for extended numerical assays #156  
- Cross-section symbology for discrete categorical assays #209  
- Assign different color for stacked plots #240  
- Add color ramp option for extended numerical plots #249  
- Add log transformation and X/Y grid displaying options in assay configuration #247  

Fixes:  

- Fix default color palette !297  
- Color ramp support for logarthmic scale #116  
- Fix uncertainty displaying with checkbox #243  
- Update correctly color ramp's min/max range #246  
- Fix error when displaying an empty extended numerical assay #250  
- Keep user-defined Collar and Trace style when loading a QGIS project #232  

For developpers:  

- cross section symbology : implementation of SplitTracesQueries and TraceSymbology classes #101  


## 0.12.1 - 2023-11-07
Fix version for twelfth alpha version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  

Fixes:  

- ensure that NULL are excluded from BDGEO filter column #241  

## 0.12.0 - 2023-10-18
Twelfth alpha version of the [OpenLog](https://apeiron.technology/openlog/) QGIS plugin 🎂  

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.  

New features:  

- Interactive legend added for stacked plots !274  
- Merge button for merging a symbology file with current one !276
- Local grid point deletion #197  
- Collar unselection from logviewer !287

Fixes:  

- Symbology file loading/merging is not case sensitive #235  
- User-defined stacked plot title is correctly displayed #231  
- Uncertainty is correctly displayed on log-scale plots #238  
- Edition of a symbology feature (e.g pattern) affect other features #237
- Fix invalid index for assay column configuration #236
- Title management improvement #231
- Inspector line bounded within plot area and text adjustment #191
- Inspector line display correctly in log scale #174

For developpers:  

- refactor CI release generation !288  


## 0.11.1 - 2023-08-01
Fix version for eleventh alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🔧.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Apply configuration to stacked #225

Fixes:

- Modified unit not taken into account for added chemical elements #222
- Error after chemical element add #221
- Error after Apply to all when only an assay column is selected #220
- French innacurate translation #216
- Pattern usgs 619 and 663 not working #215
- Stratigraphy color symbology not taken into account #213

For developpers:

- refactor/service connection use !268

## 0.11.0 - 2023-07-03
Eleventh alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Use postgresql connection service file #199
- Selection of chemical element instead of load of all element #196
- Update y scale after unit conversion #195
- Define y scale value with size conversion for core imagery #194
- Define color for categorical assay and lithology from another assay column #193
- Load and save visualization configuration as JSON file !229
- Define default visualization config file in plugin settings !233
- Add assay in tree even if no collar selected !234
- Automatic display of attribute table for collar layer !249

Fixes:

- No french translation in plugin #198
- Unit selection available on downhole data import for nominal types #186
- fix(stack): disable stack option if new added column incompatible !232
- fix(pyqtgraph): compatibility with 0.13.3 version #203 !239

For developpers:

- Feature/refactor visualization config !231
- feat(unit): refactor unit for title and range definition !230


## 0.10.0 - 2023-05-10
Tenth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Display core imagery linear view #137

Fixes:

- Invalid conversion in BDGeo for temperature unit #185

## 0.9.1 - 2023-02-21
There is a major issue when no unit is defined. User can't access downhole data visualization configuration panel.

Fixes:

- Error when no units defined #181

## 0.9.0 - 2023-02-13
Ninth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Custom projected CRS definition #103
- Support for SI units in xplordb and OpenLog #158

Fixes:

- Won't create database if date is blank #171

## 0.8.2 - 2023-02-06
Some users reported issue with file import on Windows. For better compatibility, you can now define encoding use for import.
An automatic detection of encoding is done with chardet.

Feature :

- define file encoding !206

## 0.8.1 - 2023-01-20
Fix version for the 0.8.0.

Fixes :

- data import impossible in Windows version #170

## 0.8.0 - 2023-01-12

Eight alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version focus on BDGeo support. Documentation and terminology was updated.

You can also now move your downhole data column in a specific widget or display all column in a stacked plot.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Update terminology #157
- Split graph title over 3 lines #153
- Define style for each family of BDGeo station #147
- Correct display of BDGeo assay with Chimie category #146
- Update read of available assays to add mesure category if duplicated name #145
- Distinguish nan from null entries in importer #133
- Add option to switch from line/point chart to bar chart for discrete numerical assays #122
- Add option to display assay columns in separated widget !183 !197 !196
- Documentation update !195
- Random color for assay pen !190
- Support for uncertainty display in BDGeo !189
- Access to plot options to be restricted #75

Fixes:

- Stacked entry always present in symbology options !168

For developers:

- documentation for code !182
- refactoring use of sqlalchemy for assay read and import !186

## 0.7.0 - 2022-11-07

Seventh alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This is mainly and refactoring (ui and code) and bugfixes version.

Translation support is also introduced : Bienvenue à nos utilisateurs français 🇫🇷 !

If you want to add support for a new langage, please add an issue in GitLab. We will be pleased to help you !

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.


New features:

- get collar elevation from DTM when importing collars #114
- apply a semi-random colour scheme to categorical extended assay #102
- add options to define time format in assay import #86
- add option to switch from bar to line chart for extended numerical assays #83
- UI decluterring: Move categorical data symbology control to general symbology table #110
- UI decluterring: merge symbology application button into a single drop down menu #111
- add some icons and refactor of ui (Thanks @sbeorchia !) !151
- first version of French translation !160
- limit visible assay depending on BDGeo station family #127
- add explicit option to select no DTM for collar creation #128
- Add option to delete lines in Collar creation wizard #129
- Automatic assay column name from selected import column !174
- Define specific column name for assay uncertainty !170
- Define default expand state for assay configuration !175

Fixes:

- allow use of None value in assay !159
- remove limits on index start for collar creation #124
- allow to remove definition of uncertainty and not mandatory columns #123
- import of data with null values #105
- exception raised in certain conditions when updating assay column configuration #73
- UI decluterring: merge symbology application button into a single drop down menu #
- crash in python 3.10 for categorical extended assay display !152
- Multi-column assay import wizard backtracking issue #130
- Assay importer crash on depth extended categorical data #131
- Assay importer error on duplicate field name for data and uncertainty fields #132
- Collar import fails to retrieve DTM height values #141

For developers:

- refactor OpenLogConnection #66
- refactor AssayWidget !156

## 0.6.1 - 2022-10-14

Fixes:

- Invalid assay display in data was not inserted in x value order #117
- Plugin package release link created by CI is not valid #118
- Define minimum version for some python package and display import error at plugin launch #119

## 0.6.0 - 2022-09-29

Sixth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- depth and time assay graph ordering option #97
- add inspector line to display value of assay #99
- add option to display marker for discrete assay #98
- add boxplot representation for uncertainty data #91
- add colorramp option for pen of discrete assay #81
- add categories management in assay and lithology import #64
- get collar elevation from DTM #43
- add feature for bulk collar creation #23

Fixes:

- merge available symbology when load symbology .json #108
- introduce minimum size for assay plot for correct display #107
- insertion of lithology in Xplordb !137
- data importation : remove rows with mandatory value not defined !141

## 0.5.1 - 2022-08-29

Bug fixes version for 0.5.0.

### Fixes

- error display when defining desurveying even if data is valid

## 0.5.0 - 2022-08-23

Fifth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces support for stacked assay creation, assay units and uncertainty definition.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- define assay column units, type and uncertainty
- create stacked assay
- display assay uncertainty
- scroll on assay graph with CTRL+wheel
- add length numbering option on collar creation
- ask user for assay configuration save if categorical configuration changed
- refactor tests for Spatialite and Xplordb connection

### Fixes

- catch data import exception

### Upcoming features

- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.4.0 - 2022-07-06

Fourth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces experimental support for time assay visualization and BDGeo assay visualization.

User can now save current OpenLog connection in QGIS project : for connection with authentication, a connection dialog is displayed when opening QGIS project.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- save current OpenLog connection in QGIS project : for connection with authentication, a connection dialog is displayed when opening QGIS project
- access assay visualization widget from QGIS canvas custom menu
- propagate assay visualization configuration
- simple progress dialog when importing collar assays
- connection name indicated in layers and QGIS main window title
- (experimental) support for BDGeo assay display
- (experimental) support for time assay display

### Fixes

- center vertically categorical assay text

### Upcoming features

- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.3.0 - 2022-06-03

Third alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces stratigraphy log viewer with lithology as categorical extended assay and user can define any column for assay definition.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- (experimental) stratigraphy log viewer
- (experimental) support for extended assay display (categorical and numerical)
- assay can have several column for data
- ask for connections creation after xplordb database creation
- ask for connection save after modification
- add some configuration for assay line display
- display recent spatialite database

### Fixes

- automatic desurveying after survey definition on spatialite
- some import where failing when hole_id could be imported as float

### Upcoming features

- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.2.0 - 2022-05-06

Second alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces the notion of assay and some graphical interfaces to create / import / visualize them.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- CSV import of generic assay files into spatialite and xplordb databases
- (experimental) visualization of assay in depth domain
- (experimental) connection to Acquire database for collar and survey display

### Upcoming features

- update of generic assay import to define several columns for assay definition
- stratigraphy log viewer
- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.1.0 - 2022-04-01

First alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🍾

This version features a variety of brand new experimental features as well as production-ready elements.

Although most of the development efforts have been focussed on the database-I/O-architecture trifecta, basic 3D visualization for surveys is already available. It is already possible to display desurveyed drillhole projected traces on the map.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- creation / connection to spatialite database for collar and survey storage
- creation of xplordb database on a postgresql server
- (experimental) connection to Geotic database for collar and survey display
- (experimental) connection to BDGeo database for collar display
- CSV import of collar, survey, and lithology files into spatialite and xplordb databases
- automated lithological data gap/overlap discrepancy resolution on import
- creation of collar from QGIS map
- survey table content creation for newly created collars
- 2D/3D desurveying (manual in spatialite, automatic in xplordb)

### Upcoming features

- CSV import of generic assay files into spatialite and xplordb databases
- connection to Acquire database for collar and survey display
