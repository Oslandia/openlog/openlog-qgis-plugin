# OpenLog - QGIS Plugin

A high performance drillhole visualization QGIS module with 3D, cross-sections, and strip logs capabilities.

Contribute to the project by [joining the consortium](https://apeiron.technology/openlog/)

Consult [the end-user documentation](https://geolandia.gitlab.io/openlog/openlog-documentation/index.html) and [activate notifications](https://vimeo.com/732029377) to stay up to date with our latest developments.

![openlog screenshot](https://geolandia.gitlab.io/openlog/openlog-qgis-plugin/_images/x-section_viz_03_map.png)
----

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)
