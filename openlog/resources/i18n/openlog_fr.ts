<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>@default</name>
    <message>
        <location filename="../../datamodel/connection/spatialite/spatialite_connection.py" line="326"/>
        <source>Spatialite database &apos;{filename}&apos; not available. OpenLog connection is not imported from QGIS project.</source>
        <translation>La base de données Spatialite &apos;{filename}&apos; n&apos;est pas disponible. La connexion OpenLog n&apos;est pas importée depuis le projet QGIS.</translation>
    </message>
</context>
<context>
    <name>AcquireConnection</name>
    <message>
        <location filename="../../datamodel/connection/acquire/acquire_connection.py" line="164"/>
        <source>Acquire : database : {self._connection.database} / user : {self._connection.user}</source>
        <translation>Acquire : base de données : {self._connection.database} / utilisateur : {self._connection.user}</translation>
    </message>
</context>
<context>
    <name>AcquireConnectionDialog</name>
    <message>
        <location filename="../../gui/connection/acquire_connection_dialog.py" line="20"/>
        <source>Acquire connection</source>
        <translation>Connexion Acquire</translation>
    </message>
</context>
<context>
    <name>AssayColumnCategoryTableModel</name>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_categorie.py" line="19"/>
        <source>Column</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_categorie.py" line="19"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_categorie.py" line="19"/>
        <source>Validation</source>
        <translation>Validation</translation>
    </message>
</context>
<context>
    <name>AssayColumnUncertaintyTableModel</name>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_uncertainty.py" line="48"/>
        <source>Column</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_uncertainty.py" line="33"/>
        <source>Wide interval (Î)</source>
        <translation>Intervalle simple (Δ)</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_uncertainty.py" line="48"/>
        <source>Max wide interval (Δmax)</source>
        <translation>Max. intervalle large (Δmax)</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_uncertainty.py" line="48"/>
        <source>Min wide interval (Δmin)</source>
        <translation>Min. intervalle large (Δmax)</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_uncertainty.py" line="48"/>
        <source>Max narrow interval (δmax)</source>
        <translation>Max. intervalle étroit (δmax)</translation>
    </message>
    <message>
        <location filename="../../gui/utils/mdl_assay_column_uncertainty.py" line="48"/>
        <source>Min narrow interval (δmin)</source>
        <translation>Min. intervalle étroit (δmax)</translation>
    </message>
</context>
<context>
    <name>AssayColumnVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config.py" line="44"/>
        <source>Pen</source>
        <translation>Trait</translation>
    </message>
</context>
<context>
    <name>AssayDatabaseDefinitionPageWizard</name>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="43"/>
        <source>Downhole data database definition</source>
        <translation>Définition base de données mesure</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="51"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="51"/>
        <source>Table Name</source>
        <translation>Nom table</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="124"/>
        <source>Invalid assay database definition</source>
        <translation type="obsolete">Définition base de donnée mesure invalide</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="124"/>
        <source>Check for space in assay database definition.</source>
        <translation type="obsolete">Vérifier la présence d&apos;espace dans la définition de la base de données de mesure.</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="151"/>
        <source>Invalid category table name</source>
        <translation>Nom table catégorie invalide</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="151"/>
        <source>Check for space in category table name.</source>
        <translation>Vérifier la présence d&apos;espace dans le nom de la table catégorie.</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="138"/>
        <source>Invalid downhole data database definition</source>
        <translation>Définition base de donnée mesure invalide</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_database_definition.py" line="138"/>
        <source>Check for blank characters in downhole data database definition.</source>
        <translation>Vérifier la présence d&apos;espace dans la définition de la base de données de mesure.</translation>
    </message>
</context>
<context>
    <name>AssayDatabaseDefinitionWidget</name>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="23"/>
        <source>Table</source>
        <translation>Table</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="24"/>
        <source>Hole id column</source>
        <translation>Colonne Hole id</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="25"/>
        <source>Dataset column</source>
        <translation>Colonne dataset</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="26"/>
        <source>X column</source>
        <translation>Colonne X</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="27"/>
        <source>X end column</source>
        <translation>Colonne fin X</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="28"/>
        <source>Schema</source>
        <translation>Schéma</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_database_definition_widget.py" line="33"/>
        <source>Database column</source>
        <translation>Colonne base de données</translation>
    </message>
</context>
<context>
    <name>AssayDefinitionWidget</name>
    <message>
        <location filename="../../gui/import_assay/assay_definition_widget.py" line="21"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_definition_widget.py" line="25"/>
        <source>Domain</source>
        <translation>Domaine</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/assay_definition_widget.py" line="26"/>
        <source>Extent</source>
        <translation>Etendue</translation>
    </message>
</context>
<context>
    <name>AssayImportPageWizard</name>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_import.py" line="45"/>
        <source>Downhole data values import</source>
        <translation>Importation valeurs mesure</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_import.py" line="72"/>
        <source>No category defined</source>
        <translation>Aucune catégorie définie</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_import.py" line="72"/>
        <source>Define category name for all categorical columns.</source>
        <translation>Définir le nom des catégories pour toutes les colonnes catégorielle.</translation>
    </message>
</context>
<context>
    <name>AssayImportWizard</name>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="276"/>
        <source>Assay import</source>
        <translation type="obsolete">Importation mesure</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="277"/>
        <source>Downhole data import</source>
        <translation>Importation mesure</translation>
    </message>
</context>
<context>
    <name>AssayPlotWidget</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_plot_widget.py" line="44"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
</context>
<context>
    <name>AssaySelectionPageWizard</name>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_selection.py" line="28"/>
        <source>Downhole data selection</source>
        <translation>Sélection mesure</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_selection.py" line="37"/>
        <source>Available downhole data</source>
        <translation>Mesures disponibles</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_selection.py" line="40"/>
        <source>Create downhole data</source>
        <translation>Créer une mesure</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_selection.py" line="98"/>
        <source>Downhole data variable &apos;{variable}&apos; already used.</source>
        <translation>La variable de mesure &apos;{variable}&apos; est déjà utilisée.</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/wizard/assay_selection.py" line="90"/>
        <source>Downhole data variable not defined.</source>
        <translation>Variable de mesure non définie.</translation>
    </message>
</context>
<context>
    <name>AssayVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config.py" line="201"/>
        <source>Stacked</source>
        <translation>Combinaison</translation>
    </message>
</context>
<context>
    <name>AssayVisualizationConfigTreeModel</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_tree_model.py" line="67"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_tree_model.py" line="74"/>
        <source>Collars</source>
        <translation>Forages</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_tree_model.py" line="76"/>
        <source>
        Assays</source>
        <translation type="obsolete">Mesures</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_tree_model.py" line="88"/>
        <source>Stacked</source>
        <translation>Combinaison</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_tree_model.py" line="81"/>
        <source>Downhole data</source>
        <translation>Mesures</translation>
    </message>
</context>
<context>
    <name>AssayVisualizationConfigWidget</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="44"/>
        <source>Assay</source>
        <translation type="obsolete">Mesure</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="96"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="44"/>
        <source>Collar</source>
        <translation>Forage</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.py" line="51"/>
        <source>Apply to all</source>
        <translation>Appliquer à tous</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.py" line="59"/>
        <source>Apply to visible</source>
        <translation>Appliquer aux visibles</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.py" line="69"/>
        <source>Apply to selected</source>
        <translation>Appliquer aux sélectionnés</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.py" line="107"/>
        <source>Columns</source>
        <translation>Colonnes</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.py" line="46"/>
        <source>Parameter</source>
        <translation>Paramètre</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.py" line="46"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="128"/>
        <source>Downhole data</source>
        <translation>Mesure</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="53"/>
        <source>Uncheck all</source>
        <translation>Décocher tout</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="60"/>
        <source>Check all</source>
        <translation>Cocher tout</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_visualization_config_widget.ui" line="67"/>
        <source>Collapse</source>
        <translation>Réduire</translation>
    </message>
</context>
<context>
    <name>AssayWidget</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="74"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="81"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="89"/>
        <source>Sort by collar</source>
        <translation>Tri par forage</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="97"/>
        <source>Sort by assay</source>
        <translation type="obsolete">Tri par mesure</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="102"/>
        <source>Flat grid</source>
        <translation>Grille plate</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="247"/>
        <source>Assay selection</source>
        <translation type="obsolete">Sélection mesure</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="247"/>
        <source>Select assays</source>
        <translation type="obsolete">Selection mesures</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="433"/>
        <source>Importing collar assays</source>
        <translation>Importation des mesures de forage</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="433"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="278"/>
        <source>Time assays :{0}</source>
        <translation type="obsolete">Mesures temporelles :{0}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="278"/>
        <source>Depth assays :{0}</source>
        <translation type="obsolete">Mesures en profondeur :{0}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="447"/>
        <source>Importing {0} assays</source>
        <translation>Importation des mesures {0}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="436"/>
        <source>Time assays</source>
        <translation>Mesures temporelles</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="436"/>
        <source>Depth assays</source>
        <translation>Mesures en profondeur</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="600"/>
        <source>No default configuration available for assay {}</source>
        <translation>Aucune configuration par défaut disponible pour la mesure {}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="96"/>
        <source>Sort by source</source>
        <translation>Trier par source</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="256"/>
        <source>Downhole data selection</source>
        <translation>Sélection mesure</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="256"/>
        <source>Select downhole data</source>
        <translation>Selection mesures</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="276"/>
        <source>Importing collar data</source>
        <translation>Importation des mesures de forage</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="294"/>
        <source>Time domain downhole data :{0}</source>
        <translation>Mesures temporelles :{0}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="294"/>
        <source>Depth domain downhole data:{0}</source>
        <translation>Mesures en profondeur :{0}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="308"/>
        <source>Importing {0} downhole data</source>
        <translation>Importation des mesures {0}</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="786"/>
        <source>New stacked plot</source>
        <translation>Nouvelle combinaison de graphe</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="786"/>
        <source>Stacked name</source>
        <translation>Nom combinaison</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="796"/>
        <source>Invalid stacked name</source>
        <translation>Nom de combinaison invalide</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_widget.py" line="796"/>
        <source>Stacked name already used.</source>
        <translation>Le nom de combinaison est déjà utilisé.</translation>
    </message>
</context>
<context>
    <name>BDGeoConnection</name>
    <message>
        <location filename="../../datamodel/connection/bdgeo/bdgeo_connection.py" line="105"/>
        <source>BDGeo : database : {0} / user : {1}</source>
        <translation>BDGeo : base de données : {0} / utilisateur : {1}</translation>
    </message>
</context>
<context>
    <name>BDGeoConnectionDialog</name>
    <message>
        <location filename="../../gui/connection/bdgeo_connection_dialog.py" line="20"/>
        <source>BDGeo connection</source>
        <translation>Connexion BDGeo</translation>
    </message>
</context>
<context>
    <name>BDGeoLayersInterface</name>
    <message>
        <location filename="../../datamodel/connection/bdgeo/bdgeo_layers_interface.py" line="45"/>
        <source>Stations - [{self._connection.database}]</source>
        <translation>Stations - [{self._connection.database}]</translation>
    </message>
</context>
<context>
    <name>BarSymbologyDialog</name>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_dialog.py" line="122"/>
        <source>Categorical symbology</source>
        <translation>Symbologie catégorielle</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_dialog.py" line="144"/>
        <source>Save symbology</source>
        <translation>Sauvegarde symbologie</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_dialog.py" line="144"/>
        <source>You have made changes to your symbology, do you wish to save them to a file?</source>
        <translation>Vous avez effectué des changement sur votre symbologie, voulez vous les sauvegardez dans un fichier ?</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_dialog.ui" line="14"/>
        <source>Bar symbology</source>
        <translation>Symbologie barre</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_dialog.ui" line="23"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_dialog.ui" line="30"/>
        <source>Load</source>
        <translation>Charger</translation>
    </message>
</context>
<context>
    <name>BarSymbologyMapTableModel</name>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_table_model.py" line="22"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_table_model.py" line="22"/>
        <source>Pattern</source>
        <translation>Motif</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_table_model.py" line="22"/>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/bar_symbology_table_model.py" line="22"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
</context>
<context>
    <name>CollarCreationDialog</name>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_dialog.py" line="29"/>
        <source>Collar creation</source>
        <translation>Création de forage</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_dialog.py" line="77"/>
        <source>Import exception</source>
        <translation>Exception à l&apos;import</translation>
    </message>
</context>
<context>
    <name>CollarCreationLayerModel</name>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="41"/>
        <source>EOH</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="58"/>
        <source>Collar creation</source>
        <translation>Création forage</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="175"/>
        <source>Moved point from collar layer creation model</source>
        <translation>Déplacement point depuis le model de couche de création de forage</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="328"/>
        <source>Can&apos;t define DTM value for point : {0}/{1}. z value used is 0.0.</source>
        <translation>Impossible de définir la valeur du MTN pour le point : {0}/{1}. valeur z utilisé est 0.0.</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="206"/>
        <source>Deleted point from collar layer creation model</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="41"/>
        <source>HoleID</source>
        <translation>HoleID</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="41"/>
        <source>Easting</source>
        <translation>Estant</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="41"/>
        <source>Northing</source>
        <translation>Nordant</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_layer_model.py" line="41"/>
        <source>Elevation</source>
        <translation>Elévation</translation>
    </message>
</context>
<context>
    <name>CollarCreationWidget</name>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="32"/>
        <source>Collar settings</source>
        <translation>Parametres généraux</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="38"/>
        <source>Length numbering</source>
        <translation type="obsolete">Taille chiffrage</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="52"/>
        <source>Bulk creation</source>
        <translation type="obsolete">Création en lot</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="70"/>
        <source>Extent</source>
        <translation>Etendue</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="80"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="90"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="103"/>
        <source>Rotation</source>
        <translation type="obsolete">Rotation</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="110"/>
        <source>Spacing :</source>
        <translation>Espacement :</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="123"/>
        <source>Rows</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="150"/>
        <source>Columns</source>
        <translation>Colonnes</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="160"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="174"/>
        <source>Number :</source>
        <translation>Résolution :</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="213"/>
        <source>Index start</source>
        <translation>Index de départ</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="226"/>
        <source>Index prefix</source>
        <translation>Préfixe</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="262"/>
        <source>DTM</source>
        <translation>MNT</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="276"/>
        <source>Add collars (0)</source>
        <translation>Ajouter (0)</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="351"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="351"/>
        <source>{0} collars will be created. Do you confirm ?</source>
        <translation>{0} forages vont être créer.  will be created. Confirmez vous ?</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="363"/>
        <source>Add ({0})</source>
        <translation>Ajouter ({0})</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="394"/>
        <source>Bulk collar creation</source>
        <translation>Création de grilles de sondage</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="398"/>
        <source>Can&apos;t create point on rotated grid : {0}</source>
        <translation>Impossible de créer un point sur la grille avec rotation : {0}</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="105"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="38"/>
        <source>Index base</source>
        <translation>Ordinalité</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="52"/>
        <source>Grid mode</source>
        <translation>Mode grille</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.ui" line="103"/>
        <source>Azimuth</source>
        <translation>Azimut</translation>
    </message>
    <message>
        <location filename="../../gui/collar_creation/collar_creation_widget.py" line="282"/>
        <source>Add collar</source>
        <translation>Ajouter un sondage</translation>
    </message>
</context>
<context>
    <name>CollarsImportPageWizard</name>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="25"/>
        <source>Collar import</source>
        <translation>Import forage</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="27"/>
        <source>HoleID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="28"/>
        <source>Easting</source>
        <translation>Estant</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="29"/>
        <source>Northing</source>
        <translation>Nordant</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="30"/>
        <source>Elevation</source>
        <translation>Elévation</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="31"/>
        <source>EOH</source>
        <translation>Fin du trou</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="32"/>
        <source>Survey Date</source>
        <translation>Date de levée</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="34"/>
        <source>Select a .csv file and define CRS for collars import</source>
        <translation>Sélectionner un fichier .csv et définisser le CRS pour l&apos;import des forages</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="90"/>
        <source>Collars</source>
        <translation>Forages</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="138"/>
        <source>No CRS defined</source>
        <translation>Pas de CRS de défini</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/collar.py" line="138"/>
        <source>Define imported data CRS.</source>
        <translation>Définissez le CRS des données importées.</translation>
    </message>
</context>
<context>
    <name>ColumnDefinitionTableModel</name>
    <message>
        <location filename="../../gui/utils/column_definition_tablemodel.py" line="26"/>
        <source>Column</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <location filename="../../gui/utils/column_definition_tablemodel.py" line="26"/>
        <source>Mapping</source>
        <translation type="obsolete">Correspondance</translation>
    </message>
    <message>
        <location filename="../../gui/utils/column_definition_tablemodel.py" line="26"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../../gui/utils/column_definition_tablemodel.py" line="26"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../gui/utils/column_definition_tablemodel.py" line="26"/>
        <source>Map</source>
        <translation>Association</translation>
    </message>
</context>
<context>
    <name>ConfirmationPageWizard</name>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="47"/>
        <source>Import summary</source>
        <translation>Récapitulation import</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/database_import.py" line="43"/>
        <source>Here are the data that will be imported in your connection.</source>
        <translation>Voici les données qui seront importées dans votre connexion.</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="248"/>
        <source>Import exception</source>
        <translation>Exception à l&apos;import</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="49"/>
        <source>Here are the data that will be imported into openlog connection.</source>
        <translation>Voici les données qui seront importées dans votre connexion OpenLog.</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="88"/>
        <source>Assay</source>
        <translation type="obsolete">Mesure</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="90"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="93"/>
        <source>Domain</source>
        <translation>Domaine</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="96"/>
        <source>Extent</source>
        <translation>Etendue</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="153"/>
        <source>Columns</source>
        <translation>Colonnes</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="112"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="115"/>
        <source>Table name</source>
        <translation>Nom table</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="116"/>
        <source>Name column</source>
        <translation>Colonne nom</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="166"/>
        <source>Schema</source>
        <translation>Schéma</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="130"/>
        <source>Database definition</source>
        <translation>Définition base de données</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="132"/>
        <source>Table</source>
        <translation>Table</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="137"/>
        <source>Hole ID column</source>
        <translation>Colonne Hole ID</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="142"/>
        <source>Dataset column</source>
        <translation>Colonne dataset</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="147"/>
        <source>X column</source>
        <translation>Colonne X</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="161"/>
        <source>X end column</source>
        <translation>Colonne fin X</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="178"/>
        <source>Imported data</source>
        <translation>Données importées</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="181"/>
        <source>Collar count</source>
        <translation>Nombre de forage</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="185"/>
        <source>Data count</source>
        <translation>Nombre de données</translation>
    </message>
    <message>
        <location filename="../../gui/import_assay/import_assay.py" line="88"/>
        <source>Downhole data</source>
        <translation>Mesure</translation>
    </message>
</context>
<context>
    <name>ConnectionDialog</name>
    <message>
        <location filename="../../gui/connection/connection_dialog.py" line="26"/>
        <source>OpenLog connection</source>
        <translation>Connexion OpenLog</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_dialog.py" line="66"/>
        <source>Save connection</source>
        <comment>ConnectionDialog</comment>
        <translation>Sauvegarde connexion</translation>
    </message>
    <message numerus="yes">
        <location filename="../../gui/connection/connection_dialog.py" line="55"/>
        <source>Do you want to save current connection parameters ?</source>
        <comment>ConnectionDialog</comment>
        <translation>
            <numerusform>Souhaitez-vous sauvegarder les paramètres de connexion actuels?</numerusform>
            <numerusform>Voulez vous sauvegarder les paramètres de connexion courants ?</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../gui/connection/connection_dialog.py" line="66"/>
        <source>Connection parameter changed, save changes ?</source>
        <comment>ConnectionDialog</comment>
        <translation>
            <numerusform>Paramètres de connexion modifiés, sauvegarder les modifications ?</numerusform>
            <numerusform>Paramètres de connexion modifiés, sauvegarder les modifications ?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>ConnectionPageWizard</name>
    <message>
        <location filename="../../gui/create_database/wizard/connection.py" line="16"/>
        <source>Database creation</source>
        <translation>Création base de données</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/connection.py" line="18"/>
        <source>This wizard will help you create an xplordb database</source>
        <translation>Cet assitant va vous aider pour la création d&apos;une base de données xplordb</translation>
    </message>
</context>
<context>
    <name>ConnectionWidget</name>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="38"/>
        <source>Available connections</source>
        <translation>Connexions disponibles</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="62"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="69"/>
        <source>Connection parameters</source>
        <translation>Paramètres de connexion</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="75"/>
        <source>Authentification</source>
        <translation>Authentification</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="87"/>
        <source>Test connection</source>
        <translation>Tester la connection</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="102"/>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="115"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.ui" line="128"/>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
</context>
<context>
    <name>DatabaseCreationPageWizard</name>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="34"/>
        <source>Database creation options</source>
        <translation>Options de création de base de données</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="105"/>
        <source>Database creation exception</source>
        <translation>Exception lors de la création de la base de données</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="123"/>
        <source>User &apos;{user.name}&apos; already exists.</source>
        <translation>L&apos;utilisateur &apos;{user.name}&apos; est déjà existant.</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="127"/>
        <source>User creation exception</source>
        <translation>Exception lors de la création de l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="198"/>
        <source>User creation</source>
        <translation>Création utilisateur</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="177"/>
        <source>Person creation exception</source>
        <translation>Exception lors de la creation d&apos;une personne</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="215"/>
        <source>user &apos;{user.name}&apos; : trigram is limited to 3 characters.</source>
        <translation>utilisateur &apos;{user.name}&apos; : le trigramme est limité à 3 caractères.</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="220"/>
        <source>user &apos;{user.name}&apos; : password must be defined.</source>
        <translation>utilisateur &apos;{user.name}&apos; : le mot de passe doit être défini.</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="223"/>
        <source>user &apos;{user.name}&apos; : please confirm password definition.</source>
        <translation>utilisateur &apos;{user.name}&apos; : merci de confirmer le mot de passe utilisé.</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="258"/>
        <source>Invalid name</source>
        <translation>Nom invalide</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="258"/>
        <source>Define database name.</source>
        <translation>Definissez le nom de la base de données.</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="275"/>
        <source>Create user connection</source>
        <translation>Création connexion utilisateur</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation.py" line="275"/>
        <source>Create connection for selected user ?</source>
        <translation>Créer une connexion pour les utilisateurs sélectionnés ?</translation>
    </message>
</context>
<context>
    <name>DatabaseCreationWidget</name>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation_widget.ui" line="38"/>
        <source>Database name</source>
        <translation>Nom base de données</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation_widget.ui" line="48"/>
        <source>Import sample data</source>
        <translation>Import de données d&apos;exemple</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation_widget.ui" line="55"/>
        <source>Full database</source>
        <translation>Base de données complète</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation_widget.ui" line="62"/>
        <source>User creation</source>
        <translation>Création utilisateur</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/database_creation_widget.ui" line="83"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DatabaseCreationWizard</name>
    <message>
        <location filename="../../gui/create_database/database_creation.py" line="19"/>
        <source>Xplordb database creation</source>
        <translation>Création base de données Xplordb</translation>
    </message>
</context>
<context>
    <name>DatabaseImportWizard</name>
    <message>
        <location filename="../../gui/import_data/database_import.py" line="139"/>
        <source>Data import</source>
        <translation>Import données</translation>
    </message>
</context>
<context>
    <name>DatasetSelectionPageWizard</name>
    <message>
        <location filename="../../gui/import_data/wizard/dataset.py" line="22"/>
        <source>Dataset definition</source>
        <translation>Définition source de données</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/dataset.py" line="24"/>
        <source>Select a dataset for import.</source>
        <translation>Sélection d&apos;une source de données pour l&apos;import.</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/dataset.py" line="29"/>
        <source>Dataset</source>
        <translation>Source de données</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/dataset.py" line="54"/>
        <source>Datasets</source>
        <translation>Sources de données</translation>
    </message>
</context>
<context>
    <name>DatasetSelectionWidget</name>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="87"/>
        <source>Import exception</source>
        <translation>Exception à l&apos;import</translation>
    </message>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="101"/>
        <source>Invalid database</source>
        <translation>Base de données invalide</translation>
    </message>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="129"/>
        <source>New dataset</source>
        <translation>Nouvelle source de données</translation>
    </message>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="129"/>
        <source>Dataset name</source>
        <translation>Nom source de données</translation>
    </message>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="140"/>
        <source>Invalid dataset name</source>
        <translation>Nom de source de données invalide</translation>
    </message>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="155"/>
        <source>No openlog connection</source>
        <translation>Aucune connexion openlog</translation>
    </message>
    <message>
        <location filename="../../gui/utils/dataset_selection_widget.py" line="163"/>
        <source>Dataset name is already available</source>
        <translation>Le nom de source de données n&apos;est pas disponible</translation>
    </message>
</context>
<context>
    <name>DatetimeFormatSelectionWidget</name>
    <message>
        <location filename="../../toolbelt/datetime_format_selection_widget.ui" line="38"/>
        <source>Time format</source>
        <translation>Format temps</translation>
    </message>
    <message>
        <location filename="../../toolbelt/datetime_format_selection_widget.ui" line="52"/>
        <source>Space separator</source>
        <translation>Séparateur espace</translation>
    </message>
    <message>
        <location filename="../../toolbelt/datetime_format_selection_widget.ui" line="65"/>
        <source>Time zone</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolbelt/datetime_format_selection_widget.ui" line="85"/>
        <source>Date format</source>
        <translation>Format date</translation>
    </message>
    <message>
        <location filename="../../toolbelt/datetime_format_selection_widget.ui" line="92"/>
        <source>Current time :</source>
        <translation>Temps courant :</translation>
    </message>
</context>
<context>
    <name>DelimitedTextImportWidget</name>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="756"/>
        <source>Invalid data in {col} column. {type_} expected. {exc}</source>
        <translation>Données invalides dans la colonne {col}. {type_} attendu. {exc}</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="839"/>
        <source>Can&apos;t define DTM value for point : {0}/{1}. z value used is None.</source>
        <translation>Impossible de définir la valeur du MTN pour le point : {0}/{1}. valeur z utilisé est None.</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="881"/>
        <source>Invalid column conversion</source>
        <translation>Conversion de colonne invalide</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="1014"/>
        <source>Uncertainty columns</source>
        <translation>Colonnes incertitude</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="1148"/>
        <source>Input file read failed</source>
        <translation>Erreur de lecture du fichier d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="1148"/>
        <source>Invalid file format definition. Can&apos;t read input file. Check details for more information.</source>
        <translation>Définition de format de fichier invalid. Impossible de lire le fichier d&apos;entrée. Vérifier les détails pour plus d&apos;information.</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="32"/>
        <source>Categories</source>
        <translation>Categories</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="47"/>
        <source>Sample Data</source>
        <translation>Echantillon de données</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="66"/>
        <source>Imported data</source>
        <translation>Données importées</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="82"/>
        <source>DTM</source>
        <translation>MNT</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="212"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="109"/>
        <source>Column definition</source>
        <translation>Définition colonnes</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="133"/>
        <source>add</source>
        <translation>ajouter</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="140"/>
        <source>remove</source>
        <translation>supprimer</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="166"/>
        <source>Uncertainty</source>
        <translation>Incertitude</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="175"/>
        <source>Four columns</source>
        <translation type="obsolete">Quatre colonnes</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="182"/>
        <source>Two columns</source>
        <translation type="obsolete">Deux colonnes</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="202"/>
        <source>One column</source>
        <translation type="obsolete">Une colonne</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="228"/>
        <source>CRS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="241"/>
        <source>File name</source>
        <translation>Nom fichier</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.py" line="323"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="175"/>
        <source>Boundary quad</source>
        <translation>Intervalle double</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="182"/>
        <source>Boundary pair</source>
        <translation>Intervalle libre</translation>
    </message>
    <message>
        <location filename="../../gui/utils/delimited_text_import_widget.ui" line="202"/>
        <source>Interval</source>
        <translation>Intervalle centré</translation>
    </message>
</context>
<context>
    <name>DelimiterSelectionWidget</name>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="44"/>
        <source>File Format</source>
        <translation>Format fichier</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="67"/>
        <source>The file is a comma separated value file, fields delimited by commas and quoted by &quot;</source>
        <translation>Le fichier utilise des virgules, les champs sont délimités par des virgules et encadrés par des guillements</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="70"/>
        <source>CSV (comma separated values)</source>
        <translation>CSV (virgule)</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="92"/>
        <source>Fields are defined by the specified delimiter, quote, and escape characters</source>
        <translation>Les champs sont définis par le délimiteur spécifié, les caractères de guillemet et d&apos;espace</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="95"/>
        <source>Custom delimiters</source>
        <translation>Délimiteurs personnalisés</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="179"/>
        <source>Comma character is one of the delimiters</source>
        <translation>La virgule est l&apos;un des délimiteurs</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="182"/>
        <source>Comma</source>
        <translation>Virgule</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="195"/>
        <source>Colon character is one of the delimiters</source>
        <translation>Le deux points est l&apos;un des délimiteurs</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="198"/>
        <source>Colon</source>
        <translation>Double-point</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="211"/>
        <source>Tab character is one of the delimiters</source>
        <translation>La tabulation est l&apos;un des délimiteurs</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="214"/>
        <source>Tab</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="230"/>
        <source>Semicolon character is one of the delimiters</source>
        <translation>Le point-virgule est l&apos;un des délimiteurs</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="233"/>
        <source>Semicolon</source>
        <translation>Point-virgule</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="246"/>
        <source>Space character is one of the delimiters</source>
        <translation>L&apos;espace est l&apos;un des délimiteurs</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="249"/>
        <source>Space</source>
        <translation>Espace</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="267"/>
        <source>Others</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="301"/>
        <source>Delimiters to use when splitting fields in the text file. The delimiter can be more than one character. These characters are used in addition to the comma, tab, space, colon, and semicolon options.</source>
        <translation>Délimiteurs a utilisés pour séparer les champs dans le fichier texte. Le délimiteur peut faire plus d&apos;un caractère. Ces caractères sont utilisés en plus des virgules, tabulation, double-point et point-virfule.</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="323"/>
        <source>Quote</source>
        <translation>Guillement</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="348"/>
        <source>The quote character(s) enclose fields which may include delimiters and new lines</source>
        <translation>Les caractères de guillemets encadrent les champs qui pourraient contenir des caractères de retour à la ligne ou des délimiteurs</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="408"/>
        <source>&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="380"/>
        <source>Escape</source>
        <translation>Echappement</translation>
    </message>
    <message>
        <location filename="../../toolbelt/delimiter_selection_widget.ui" line="405"/>
        <source>The escape character(s) force the next character to be treated as a normal character (that is not a delimiter, quote, or new line character). If the escape character is the same as a quote character, it only escapes itself and only within quotes.</source>
        <translation>Le (ou les) caractère(s) échappé(s) force(nt) le prochain caractère à être considéré comme un caractère normal (c&apos;est-à-dire qui n&apos;est pas un délimiteur, un guillemet ou un caractère de retour chariot). Si le caractère échappé est le même qu&apos;un caractère de guillemet, il s&apos;échappe lui-même et uniquement entre guillemets.</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.ui" line="62"/>
        <source>Dataset</source>
        <translation>Source de données</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.ui" line="26"/>
        <source>Person</source>
        <translation>Personne</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.ui" line="36"/>
        <source>Collars</source>
        <translation>Forages</translation>
    </message>
</context>
<context>
    <name>DialogSettingsWidget</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="75"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="96"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="105"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode débogage (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="155"/>
        <source>Version used to save settings:</source>
        <translation>Version utilisée pour sauvegarder les paramètres:</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="177"/>
        <source>Help</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="199"/>
        <source>Report an issue</source>
        <translation>Signaler un problème</translation>
    </message>
</context>
<context>
    <name>DiscreteAssayColumnVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="211"/>
        <source>Uncertainty</source>
        <translation>Incertitude</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="56"/>
        <source>Symbol</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="64"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="68"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="91"/>
        <source>Color ramp</source>
        <translation>Palette de couleur</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="82"/>
        <source>Color map name</source>
        <translation>Nom palette</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="93"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="97"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="101"/>
        <source>Use color ramp for pen</source>
        <translation>Utiliser la palette de couleur comme trait</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="201"/>
        <source>Switch to extended</source>
        <translation>Mode histogramme</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/discrete/config.py" line="197"/>
        <source>Switch to discrete</source>
        <translation>Mode courbe</translation>
    </message>
</context>
<context>
    <name>ExtendedAssayColumnVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="35"/>
        <source>Bar pen</source>
        <translation>Trait barre</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="36"/>
        <source>Bar color</source>
        <translation>Couleur barre</translation>
    </message>
</context>
<context>
    <name>ExtendedCategoricalAssayColumnVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="224"/>
        <source>Bar pen</source>
        <translation>Trait barre</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="226"/>
        <source>Display category</source>
        <translation>Affichage categorie</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="229"/>
        <source>Text color</source>
        <translation>Couleur texte</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="232"/>
        <source>Symbology</source>
        <translation>Symbologie</translation>
    </message>
</context>
<context>
    <name>ExtendedNominalAssayColumnVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="169"/>
        <source>Text color</source>
        <translation>Couleur texte</translation>
    </message>
</context>
<context>
    <name>ExtendedNumericalAssayColumnVisualizationConfig</name>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="105"/>
        <source>Switch to discrete</source>
        <translation>Passage en discret</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/extended/config.py" line="101"/>
        <source>Switch to extended</source>
        <translation>Passage en étendu</translation>
    </message>
</context>
<context>
    <name>GeoticConnection</name>
    <message>
        <location filename="../../datamodel/connection/geotic/geotic_connection.py" line="155"/>
        <source>Geotic : database : {self._connection.database} / user : {self._connection.user}</source>
        <translation>Geotic : base de données : {self._connection.database} / utilisateur : {self._connection.user}</translation>
    </message>
</context>
<context>
    <name>GeoticConnectionDialog</name>
    <message>
        <location filename="../../gui/connection/geotic_connection_dialog.py" line="20"/>
        <source>Geotic connection</source>
        <translation>Connextion Geotic</translation>
    </message>
</context>
<context>
    <name>IntersectedPoint</name>
    <message>
        <location filename="../../gui/assay_visualization/assay_inspector_line.py" line="337"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/assay_inspector_line.py" line="340"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>InvalidInterface</name>
    <message>
        <location filename="../../datamodel/connection/interfaces/layers_interface.py" line="87"/>
        <source>Collar</source>
        <translation>Forage</translation>
    </message>
    <message>
        <location filename="../../datamodel/connection/interfaces/layers_interface.py" line="96"/>
        <source>Trace</source>
        <translation>Trace</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.py" line="66"/>
        <source>Default database</source>
        <translation>Base de données par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.py" line="229"/>
        <source>Connection failed</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.py" line="253"/>
        <source>New connection</source>
        <translation>Nouvelle connexion</translation>
    </message>
    <message>
        <location filename="../../gui/connection/connection_widget.py" line="253"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>LithsImportPageWizard</name>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="115"/>
        <source>Lithology import</source>
        <translation>Import lithologie</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="117"/>
        <source>HoleID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="118"/>
        <source>LithCode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="119"/>
        <source>From_m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="120"/>
        <source>To_m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="122"/>
        <source>Select a .csv file for lithology import</source>
        <translation>Sélectionner un fichier .csv pour import lithologie</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="129"/>
        <source>Import options</source>
        <translation>Options import</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="136"/>
        <source>Gap resolution</source>
        <translation>Résolution gap</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="150"/>
        <source>Overlap resolution</source>
        <translation>Résolution chevauchement</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/lith.py" line="222"/>
        <source>Lithologies</source>
        <translation>Lithologies</translation>
    </message>
</context>
<context>
    <name>MssqlConnectionWidget</name>
    <message>
        <location filename="../../gui/connection/mssql_connection_widget.py" line="15"/>
        <source>Secure connection</source>
        <translation>Connexion sécurisée</translation>
    </message>
</context>
<context>
    <name>OpenLogConnectionFactory</name>
    <message>
        <location filename="../../datamodel/connection/openlog_connection_factory.py" line="60"/>
        <source>OpenLog connection type &apos;{connection_type}&apos; not available. No connection loaded from QGIS project.</source>
        <translation>Le type de connexion OpenLog &apos;{connection_type}&apos; n&apos;est pas disponible. Aucune connexion n&apos;est chargée depuis le projet QGIS.</translation>
    </message>
</context>
<context>
    <name>OpenLogProvider</name>
    <message>
        <location filename="../../processing/provider.py" line="48"/>
        <source>OpenLog</source>
        <translation>OpenLog</translation>
    </message>
    <message>
        <location filename="../../processing/provider.py" line="58"/>
        <source>OpenLog - Tools</source>
        <translation>OpenLog - Outils</translation>
    </message>
</context>
<context>
    <name>OpenlogPlugin</name>
    <message>
        <location filename="../../plugin_main.py" line="232"/>
        <source>Error importing dependencies : {}.
 Plugin disabled.</source>
        <translation>Erreur à l&apos;import des dépendances : {}.
 Le plugin est désactivé.</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="243"/>
        <source>OpenLog Plugin disabled. Please install all dependencies and then restart QGIS.</source>
        <translation>Le plugin OpenLog est désactivé. Merci d&apos;installer toutes les dépendances et ensuite redémarrer QGIS.</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="255"/>
        <source>Dependencies satisfied</source>
        <translation>Dépendances résolues</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="268"/>
        <source>Help</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="277"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="288"/>
        <source>Database management</source>
        <translation>Gestion base de données</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="306"/>
        <source>Import collar data</source>
        <translation>Import données de forage</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="316"/>
        <source>Import survey data</source>
        <translation>Import données de levés</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="351"/>
        <source>Create new xplordb database</source>
        <translation>Créer une nouvelle base de données xplordb</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="359"/>
        <source>Create new spatialite database</source>
        <translation>Créer une nouvelle base de données spatialite</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="367"/>
        <source>Connect to database</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="374"/>
        <source>Connect to xplordb database</source>
        <translation type="obsolete">Se connecter à une base de données xplordb</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="382"/>
        <source>Connect to geotic database</source>
        <translation type="obsolete">Se connecter à une base de données geotic</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="390"/>
        <source>Connect to acquire database</source>
        <translation type="obsolete">Se connecter à une base de données acquire</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="398"/>
        <source>Spatialite</source>
        <translation>Base Spatialite</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="409"/>
        <source>Open...</source>
        <translation>Ouvrir...</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="422"/>
        <source>Connect to BD Geo database</source>
        <translation type="obsolete">Se connecter à une base de données BD Geo</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="431"/>
        <source>Add collar</source>
        <translation>Ajouter un forage</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="439"/>
        <source>Desurvey</source>
        <translation>Interpoler les levés de sondage</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="449"/>
        <source>Define surveys for selected holes</source>
        <translation type="obsolete">Définition trace pour forage sélectionnés</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="465"/>
        <source>Depth assay visualization</source>
        <translation type="obsolete">Visualisation mesure en profondeur</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="478"/>
        <source>Time assay visualization</source>
        <translation type="obsolete">Visualisation mesure temporelle</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="669"/>
        <source>OpenLog connection layers can&apos;t be removed.</source>
        <translation>Les couches de la connexion OpenLog ne peuvent pas être supprimées.</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="857"/>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="338"/>
        <source>Import downhole data</source>
        <translation>Import données de mesure</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="477"/>
        <source>Display depth data</source>
        <translation>Affichage des données de forages</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="490"/>
        <source>Display time data</source>
        <translation>Affichage des données temporelles</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="374"/>
        <source>xplordb</source>
        <translation>base xplordb</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="382"/>
        <source>Geotic</source>
        <translation>base Geotic</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="390"/>
        <source>Acquire</source>
        <translation>base Acquire</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="422"/>
        <source>BD Geo</source>
        <translation>base BD Geo</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="449"/>
        <source>Edit surveys</source>
        <translation>Editer les levés de sondage</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="374"/>
        <source>xplordb</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="382"/>
        <source>Geotic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="390"/>
        <source>Acquire</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="422"/>
        <source>BD Geo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="449"/>
        <source>Edit surveys</source>
        <translation>Edition des traces</translation>
    </message>
</context>
<context>
    <name>PersonSelectionPageWizard</name>
    <message>
        <location filename="../../gui/import_data/wizard/person.py" line="22"/>
        <source>Person definition</source>
        <translation>Définition personne</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/person.py" line="24"/>
        <source>Select a person for import.</source>
        <translation>Sélectionner une personne pour l&apos;import.</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/person.py" line="29"/>
        <source>Person</source>
        <translation>Personne</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/person.py" line="51"/>
        <source>Persons</source>
        <translation>Personnes</translation>
    </message>
</context>
<context>
    <name>PersonSelectionWidget</name>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="84"/>
        <source>Import exception</source>
        <translation>Exception à l&apos;import</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="99"/>
        <source>Invalid database</source>
        <translation>Base de données invalide</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="118"/>
        <source>New person</source>
        <translation>Nouvelle personne</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="118"/>
        <source>Person code</source>
        <translation>Code personne</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="129"/>
        <source>Invalid person code</source>
        <translation>Code personne invalide</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="144"/>
        <source>No openlog connection</source>
        <translation>Aucune connexion openlog</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="155"/>
        <source>Person code is limited to {person_code_max_size}</source>
        <translation>Le code personne est limité à {person_code_max_size}</translation>
    </message>
    <message>
        <location filename="../../gui/utils/person_selection_widget.py" line="158"/>
        <source>Person code is already available</source>
        <translation>Le code personne n&apos;est pas disponible</translation>
    </message>
</context>
<context>
    <name>PivotSpatialiteLayersInterface</name>
    <message>
        <location filename="../../datamodel/connection/spatialite/pivot_spatialite_layers_interface.py" line="53"/>
        <source>Collar - [{0}] (pivot spatialite)</source>
        <translation>Forage - [{0}] (pivot spatialite)</translation>
    </message>
    <message>
        <location filename="../../datamodel/connection/spatialite/pivot_spatialite_layers_interface.py" line="79"/>
        <source>Trace - [{0}] (pivot spatialite)</source>
        <translation>Trace - [{0}] (pivot spatialite)</translation>
    </message>
</context>
<context>
    <name>PlgTranslator</name>
    <message>
        <location filename="../../toolbelt/translator.py" line="68"/>
        <source>Your selected locale ({}) is not available. Please consider to contribute with your own translation :). Contact the plugin maintener(s): {}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PlotGrid</name>
    <message>
        <location filename="../../gui/assay_visualization/plot_grid.py" line="80"/>
        <source>Enable</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/plot_grid.py" line="85"/>
        <source>Synchronization</source>
        <translation>Synchronisation</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/plot_grid.py" line="99"/>
        <source>Inspector line</source>
        <translation>Ligne inspection</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/plot_grid.py" line="360"/>
        <source>Depth</source>
        <translation type="obsolete">Profondeur</translation>
    </message>
</context>
<context>
    <name>RotatedGridPointCreationAlgorihtm</name>
    <message>
        <location filename="../../processing/rotated_grid_point_creation.py" line="130"/>
        <source>{0} and {1] not defined. Can&apos;t define horizontal spacing for grid</source>
        <translation>{0} et {1] ne sont pas définis. Impossible de définir l&apos;espace horizontal pour la grille</translation>
    </message>
    <message>
        <location filename="../../processing/rotated_grid_point_creation.py" line="145"/>
        <source>{0} and {1] not defined. Can&apos;t define vertical spacing for grid</source>
        <translation>{0} et {1] ne sont pas définis. Impossible de définir l&apos;espace vertical pour la grille</translation>
    </message>
</context>
<context>
    <name>SpatialiteConnection</name>
    <message>
        <location filename="../../datamodel/connection/spatialite/spatialite_connection.py" line="266"/>
        <source>Exception raised for collar trigger creation : {}</source>
        <translation>Exception lors de la création du trigger de création de forage : {}</translation>
    </message>
    <message>
        <location filename="../../datamodel/connection/spatialite/spatialite_connection.py" line="283"/>
        <source>Spatialite : {0}</source>
        <translation>Spatialite : {0}</translation>
    </message>
</context>
<context>
    <name>SpatialiteLayersInterface</name>
    <message>
        <location filename="../../datamodel/connection/spatialite/spatialite_layers_interface.py" line="61"/>
        <source>Trace - [{0}]</source>
        <translation>Trace - [{0}]</translation>
    </message>
    <message>
        <location filename="../../datamodel/connection/spatialite/spatialite_layers_interface.py" line="43"/>
        <source>Collar - [{0}]</source>
        <translation>Forage - [{0}]</translation>
    </message>
</context>
<context>
    <name>SqlAlchemyAssayInterface</name>
    <message>
        <location filename="../../datamodel/connection/sqlalchemy/sqlalchemy_assay_interface.py" line="129"/>
        <source>Exception raised for assay tables creation. It must come from already created tables : {}.</source>
        <translation>Exception lors de la création des tables de mesure. Cela doit prevenir de tables déja crées : {}.</translation>
    </message>
</context>
<context>
    <name>StackConfigTableModel</name>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_table_model.py" line="26"/>
        <source>Hole ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_table_model.py" line="24"/>
        <source>Assay</source>
        <translation type="obsolete">Mesure</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_table_model.py" line="26"/>
        <source>Column</source>
        <translation>Colonne</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_table_model.py" line="26"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_table_model.py" line="26"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_table_model.py" line="26"/>
        <source>Downhole data</source>
        <translation>Mesure</translation>
    </message>
</context>
<context>
    <name>StackedConfigCreationWidget</name>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_creation_widget.py" line="70"/>
        <source>Invalid stacked. Check if multiple unit or series type are used. ({invalid_series_name}) series types are not supported.</source>
        <translation>Combinaison invalid. Vérifier si des unités ou type de series différentes sont utilisées. ({invalid_series_name}) les types de séries ne sont pas supportées.</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_creation_widget.ui" line="32"/>
        <source>Stacked</source>
        <translation>Combinaison</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_creation_widget.ui" line="38"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
</context>
<context>
    <name>StackedConfigWidget</name>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_widget.ui" line="85"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_widget.ui" line="34"/>
        <source>Uncheck all</source>
        <translation>Décocher tout</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_widget.ui" line="41"/>
        <source>Check all</source>
        <translation>Cocher tout</translation>
    </message>
    <message>
        <location filename="../../gui/assay_visualization/stacked/stacked_config_widget.ui" line="48"/>
        <source>Collapse</source>
        <translation>Réduire</translation>
    </message>
</context>
<context>
    <name>SurveyCreationDialog</name>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.py" line="28"/>
        <source>Survey creation/edition</source>
        <translation>Création de levé de sondage</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.py" line="77"/>
        <source>Null data entries</source>
        <translation>Données non définies</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.py" line="77"/>
        <source>Null data entries found, press OK to discard them and proceed.</source>
        <translation>Certaines données ne sont pas définies, appuyer sur OK pour les exclure et continuer l&apos;import.</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_dialog.py" line="116"/>
        <source>Import exception</source>
        <translation>Exception à l&apos;import</translation>
    </message>
</context>
<context>
    <name>SurveyCreationLayerModel</name>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_model.py" line="20"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_model.py" line="20"/>
        <source>Dip</source>
        <translation>Inclinaison</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_model.py" line="20"/>
        <source>Azimuth</source>
        <translation>Azimut</translation>
    </message>
</context>
<context>
    <name>SurveyCreationWidget</name>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_widget.ui" line="32"/>
        <source>Survey</source>
        <translation>Levé</translation>
    </message>
    <message>
        <location filename="../../gui/survey_creation/survey_creation_widget.ui" line="53"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>SurveysImportPageWizard</name>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="27"/>
        <source>Survey import</source>
        <translation>Import levé</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="29"/>
        <source>HoleID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="30"/>
        <source>Dip</source>
        <translation>Inclinaison</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="31"/>
        <source>Azimuth</source>
        <translation>Azimut</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="32"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="34"/>
        <source>Select a .csv file for surveys import</source>
        <translation>Sélectionner un fichier .csv pour l&apos;import des levés</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="41"/>
        <source>Import options</source>
        <translation>Options import</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="44"/>
        <source>Invert Dips</source>
        <translation>Inversion inclinaison</translation>
    </message>
    <message>
        <location filename="../../gui/import_data/wizard/survey.py" line="111"/>
        <source>Surveys</source>
        <translation>Levés</translation>
    </message>
</context>
<context>
    <name>UserTableModel</name>
    <message>
        <location filename="../../gui/create_database/wizard/user_tablemodel.py" line="61"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/user_tablemodel.py" line="61"/>
        <source>Trigram</source>
        <translation>Trigramme</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/user_tablemodel.py" line="61"/>
        <source>Role</source>
        <translation>Role</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/user_tablemodel.py" line="61"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../../gui/create_database/wizard/user_tablemodel.py" line="61"/>
        <source>Confirm password</source>
        <translation>Confirmer le mot de passe</translation>
    </message>
</context>
<context>
    <name>XplordbConnection</name>
    <message>
        <location filename="../../datamodel/connection/xplordb/xplordb_connection.py" line="197"/>
        <source>Xplordb : database : {self._connection.database} / user : {self._connection.user}</source>
        <translation>Xplordb : base de données : {self._connection.database} / utilisateur : {self._connection.user}</translation>
    </message>
</context>
<context>
    <name>XplordbConnectionDialog</name>
    <message>
        <location filename="../../gui/connection/xplordb_connection_dialog.py" line="26"/>
        <source>Xplordb connection</source>
        <translation>Connexion Xplordb</translation>
    </message>
    <message>
        <location filename="../../gui/connection/xplordb_connection_dialog.py" line="39"/>
        <source>Invalid database</source>
        <translation>Base de données invalide</translation>
    </message>
    <message>
        <location filename="../../gui/connection/xplordb_connection_dialog.py" line="39"/>
        <source>xplordb schema not available.
 Do you want to install lite version ?</source>
        <translation>schema xplordb non disponible.
 Voulez vous installer la version légère ?</translation>
    </message>
    <message>
        <location filename="../../gui/connection/xplordb_connection_dialog.py" line="69"/>
        <source>Xplordb creation error</source>
        <translation>Erreur création Xplordb</translation>
    </message>
</context>
<context>
    <name>XplordbLayersInterface</name>
    <message>
        <location filename="../../datamodel/connection/xplordb/xplordb_layers_interface.py" line="42"/>
        <source>Collar - [{self._connection.database}]</source>
        <translation>Forage - [{self._connection.database}]</translation>
    </message>
    <message>
        <location filename="../../datamodel/connection/xplordb/xplordb_layers_interface.py" line="66"/>
        <source>Trace - [{self._connection.database}]</source>
        <translation>Trace - [{self._connection.database}]</translation>
    </message>
</context>
</TS>
