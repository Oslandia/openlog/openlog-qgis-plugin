from qgis.PyQt import QtCore
from qgis.PyQt.QtCore import QVariant
from qgis.PyQt.QtGui import QColor

from openlog.toolbelt.vertical_string_map_table_model import VerticalStringMapTableModel


class BarSymbologyColorTableModel(VerticalStringMapTableModel):
    def __init__(self, parent=None) -> None:
        """
        QStandardItemModel for BarSymbology color dict display

        Args:
            parent: QWidget
        """
        super().__init__(parent=parent)
        key_label = self.tr("Key")
        val_label = self.tr("Color")

        self.setHorizontalHeaderLabels([key_label, val_label])

    def data(
        self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole
    ) -> QVariant:
        """
        Override StringMapTableModel data() for :
        - icon of pattern col

        Args:
            index: QModelIndex
            role: Qt role

        Returns: QVariant

        """
        result = super().data(index, role)

        if role == QtCore.Qt.BackgroundRole and index.column() == self.VALUE_COL:
            html_color = str(self.data(index, QtCore.Qt.DisplayRole))
            result = QColor(html_color)

        return result
