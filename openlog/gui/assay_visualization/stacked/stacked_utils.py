from typing import Any, List

from openlog.datamodel.assay.generic_assay import AssaySeriesType


def invalid_series_type() -> List[AssaySeriesType]:
    return [AssaySeriesType.CATEGORICAL, AssaySeriesType.NOMINAL]


def is_config_list_valid_for_stack(config_list: List[Any]) -> bool:
    """
    Check if list of AssayColumnVisualizationConfig is valid :
    - only one unit used
    - only one series type used
    - only valid series type used
    - no plugin assay column is used

    Returns: True if list is valid, False otherwise

    """
    valid = True

    # no plugin assay column
    valid = valid and not any(
        [c.column.series_type == AssaySeriesType.PLUGIN for c in config_list]
    )

    # Get all units
    units = set([c.column.unit for c in config_list])
    valid = valid and len(units) == 1

    # Get all series_type
    series_type = set([c.column.series_type for c in config_list])
    valid = valid and len(series_type) == 1

    for invalid_serie in invalid_series_type():
        valid = valid and invalid_serie not in series_type
    return valid


def config_list_default_unit(config_list: List[Any]) -> str:
    """
    Returns default unit from config list
    - first unit is used

    Returns: first unit found

    """
    units = [c.column.unit for c in config_list]
    if len(units):
        unit = units[0]
    else:
        unit = ""
    return unit
