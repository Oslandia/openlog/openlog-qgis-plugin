import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes
from qgis.PyQt import QtWidgets


class BoldBoolParameterItem(pTypes.WidgetParameterItem):
    def makeWidget(self):
        # bold
        f = self.font(0)
        f.setBold(True)
        self.setFont(0, f)
        w = QtWidgets.QCheckBox()
        w.sigChanged = w.toggled
        w.value = w.isChecked
        w.setValue = w.setChecked
        self.hideWidget = False
        return w


class BoldBoolParameter(ptree.Parameter):
    """
    Custom parameter (boolean with checkbox) with bold label.
    """

    def makeTreeItem(self, depth):
        """
        Return a TreeWidgetItem suitable for displaying/controlling the content of
        this parameter. This is called automatically when a ParameterTree attempts
        to display this Parameter.
        Most subclasses will want to override this function.
        """
        # Default to user-specified itemClass. If not present, check for a registered item class. Finally,
        # revert to ParameterItem if both fail
        itemClass = BoldBoolParameterItem
        return itemClass(self, depth)
