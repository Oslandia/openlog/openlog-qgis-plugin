from pyqtgraph.parametertree.parameterTypes import ActionParameter, ActionParameterItem
from pyqtgraph.parametertree.parameterTypes.action import (
    ParameterControlledButton,
    ParameterItem,
)
from qgis.PyQt import QtCore, QtGui, QtWidgets
from qgis.PyQt.QtCore import QSize, Qt


class CustomActionParameterItem(ActionParameterItem):
    """
    Subclass of ActionParameterItem with more control on button appearance and alignment.
    Added parameters are button width, height and tooltip.
    """

    def __init__(self, param, depth):
        ParameterItem.__init__(self, param, depth)
        self.layoutWidget = QtWidgets.QWidget()
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layoutWidget.setLayout(self.layout)
        self.button = ParameterControlledButton(param, self.layoutWidget)
        if "width" in param.opts.keys():
            self.button.setFixedWidth(param.opts["width"])
        if "height" in param.opts.keys():
            self.button.setFixedHeight(param.opts["height"])

        self.button.setIconSize(self.button.size())

        if "tooltip" in param.opts.keys():
            self.button.setToolTip(param.opts["tooltip"])

        self.layout.addWidget(self.button, alignment=Qt.AlignLeft)
        self.titleChanged()


class CustomActionParameter(ActionParameter):
    itemClass = CustomActionParameterItem
