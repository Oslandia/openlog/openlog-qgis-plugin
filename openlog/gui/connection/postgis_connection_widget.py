import psycopg2

from openlog.datamodel.connection.openlog_connection import Connection
from openlog.gui.connection.connection_widget import ConnectionWidget


class PostgisConnectionWidget(ConnectionWidget):
    def _get_connection_type_name(self) -> str:
        """
        Returns connection type name for save in QgsSettings

        Returns: 'postgis'

        """
        return "postgis"

    def get_connection_object(self, connection: Connection):
        """
        Create a psycog2 connection.
        Raises InvalidConnection if connection invalid

        Returns: psycog2 connection

        """
        try:
            args = connection.get_postgres_args()

            conn = psycopg2.connect(**args)
        except psycopg2.Error as exc:
            raise ConnectionWidget.InvalidConnection(exc)
        return conn
