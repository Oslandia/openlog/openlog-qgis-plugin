import pyqtgraph as pg
import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes

from openlog.gui.assay_visualization.config import json_utils
from openlog.gui.assay_visualization.extended.config import (
    ExtendedAssayColumnVisualizationConfig,
)
from openlog.plugins.visualization.nominal.saver import ExtendedNominalSaver


class ExtendedNominalAssayColumnVisualizationConfig(
    ExtendedAssayColumnVisualizationConfig
):
    saverClass = ExtendedNominalSaver

    def __init__(self, column: str):
        """
        Store visualization configuration for an extended nominal assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : black)
        - bar_color_param (QColor): assay column bar color (default : white)
        - text_color_param (QColor): assay column text color (default : black)

        Args:
            column: (str) assay column name
        """
        super().__init__(column)

        self.is_splittable = False
        self.is_discrete = False
        self.is_minmax_registered = False
        self.is_switchable_config = False
        self.is_categorical = False

        self.text_color_param = pTypes.ColorParameter(
            name=self.tr("Text color"), value="black", default="black"
        )

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            # Define current parameters
            self.plot_item.setTextColor(self.text_color_param.value())

            # Connection to parameter changes
            self.text_color_param.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setTextColor(changes)
            )

    def add_children_to_root_param(self, params: ptree.Parameter):

        params.addChild(self.text_color_param)
        super().add_children_to_root_param(params)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        self.text_color_param.setValue(other.text_color_param.value())
