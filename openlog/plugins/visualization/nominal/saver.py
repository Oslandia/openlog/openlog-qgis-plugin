from qgis.PyQt.QtGui import QColor

from openlog.gui.assay_visualization.config.config_saver import ConfigSaver
from openlog.gui.assay_visualization.config.config_saver_utils import (
    deserialize_pen_param,
    serialize_pen_param,
)


class ExtendedNominalSaver(ConfigSaver):
    def _serialize(self, config):

        d = super()._serialize(config)
        # bar pen
        d |= serialize_pen_param(config, "pen_params", "bar_pen")
        # text color
        d["text_color"] = config.text_color_param.value().name()
        # bar fill
        d["bar_color"] = config.bar_color_param.value().name()

        return d

    def _deserialize(self, config, dict):

        # bar pen
        deserialize_pen_param(config, dict, "pen_params", "bar_pen")
        config._pen_updated()
        # text color
        qcolor = QColor()
        qcolor.setNamedColor(dict.get("text_color"))
        config.text_color_param.setValue(qcolor)
        # bar fill
        qcolor = QColor()
        qcolor.setNamedColor(dict.get("bar_color"))
        config.bar_color_param.setValue(qcolor)
