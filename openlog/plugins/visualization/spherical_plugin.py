import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDefinition,
    GenericAssay,
)

# plugin
from openlog.plugins.hookspecs import SerieTypeHook, hookimpl
from openlog.plugins.visualization.structural.config import (
    DiscreteSphericalAssayColumnVisualizationConfig,
)
from openlog.plugins.visualization.structural.item import (
    create_discrete_plot_items_from_extended_structural,
    create_discrete_structural_plot_items,
)

PLUGIN_NAME = "spherical"


# SPHERICAL
def create_assay_visualization_column_config(
    extent: AssayDataExtent, assay_definition: AssayDefinition, column_name: str
):

    assay_column = assay_definition.columns[column_name]

    if extent == AssayDataExtent.DISCRETE:
        return DiscreteSphericalAssayColumnVisualizationConfig(assay_column)
    else:
        return DiscreteSphericalAssayColumnVisualizationConfig(assay_column)


def create_plot_item(plot: pg.PlotWidget, assay: GenericAssay, column: str, config):

    if assay.assay_definition.data_extent == AssayDataExtent.DISCRETE:
        return create_discrete_structural_plot_items(plot, assay, column, config)

    elif assay.assay_definition.data_extent == AssayDataExtent.EXTENDED:
        return create_discrete_plot_items_from_extended_structural(
            plot, assay, column, config
        )


@hookimpl
def serie_type():
    return SerieTypeHook(
        name=PLUGIN_NAME,
        create_assay_visualization_column_config=create_assay_visualization_column_config,
        create_plot_item=create_plot_item,
    )
