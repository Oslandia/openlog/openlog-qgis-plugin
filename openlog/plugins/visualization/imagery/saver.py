from openlog.gui.assay_visualization.config.config_saver import ConfigSaver


class ExtendedImagerySaver(ConfigSaver):
    def _serialize(self, config):

        d = super()._serialize(config)
        d["min_width_px"] = config.min_width_px_param.value()
        d["max_stretch_factor"] = config.max_stretch_factor_param.value()

        return d

    def _deserialize(self, config, dict):

        config.min_width_px_param.setValue(dict.get("min_width_px"))
        config.max_stretch_factor_param.setValue(dict.get("max_stretch_factor"))
