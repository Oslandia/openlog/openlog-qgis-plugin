import pyqtgraph as pg
import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes

from openlog.gui.assay_visualization.config.assay_column_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.plugins.visualization.imagery.saver import ExtendedImagerySaver


class ExtendedImageryColumnVisualizationConfig(AssayColumnVisualizationConfig):
    saverClass = ExtendedImagerySaver

    def __init__(self, column: str):
        """
        Store visualization configuration for an extended imagery assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - max_ratio_param (float): maximum ratio
        - min_width_px_param (int): minimum image width in pixels

        Args:
            column: (str) assay column name
        """
        super().__init__(column)

        self.is_splittable = False
        self.is_discrete = False
        self.is_minmax_registered = False
        self.is_switchable_config = False
        self.is_categorical = False

        # Remove pen parameter
        self.pen_params = None

        self.min_width_px_param = pTypes.SimpleParameter(
            name=self.tr("Min. width (px)"),
            type="int",
            value=150,
            default=150,
            min=50,
            max=300,
        )
        self.max_stretch_factor_param = pTypes.SimpleParameter(
            name=self.tr("Max. stretch factor"),
            type="int",
            value=1,
            default=1,
            min=1,
            max=20,
        )

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            # Define current parameters
            self.plot_item.set_min_width(self.min_width_px_param.value())
            self.plot_item.set_max_stretch_factor(self.max_stretch_factor_param.value())

            # Connection to parameter changes
            self.min_width_px_param.sigValueChanged.connect(
                lambda params, changes: self.plot_item.set_min_width(changes)
            )
            self.max_stretch_factor_param.sigValueChanged.connect(
                lambda params, changes: self.plot_item.set_max_stretch_factor(changes)
            )

    def add_children_to_root_param(self, params: ptree.Parameter):

        params.addChild(self.min_width_px_param)
        params.addChild(self.max_stretch_factor_param)
        super().add_children_to_root_param(params)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        self.min_width_px_param.setValue(other.min_width_px_param.value())
        self.max_stretch_factor_param.setValue(other.max_stretch_factor_param.value())
