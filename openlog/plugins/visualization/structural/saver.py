from openlog.gui.assay_visualization.config.config_saver import ConfigSaver


class DiscreteStructuralSaver(ConfigSaver):
    def _serialize(self, config):

        d = super()._serialize(config)
        # symbol size
        d["symbol_size"] = config.symbol_size_parameter.value()

        return d

    def _deserialize(self, config, dict):

        # symbol size
        config.symbol_size_parameter.setValue(dict.get("symbol_size"))
