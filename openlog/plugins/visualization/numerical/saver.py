import pyqtgraph as pg
from qgis.PyQt.QtGui import QColor

from openlog.gui.assay_visualization.config.config_saver import ConfigSaver
from openlog.gui.assay_visualization.config.config_saver_utils import (
    deserialize_pen_param,
    serialize_pen_param,
)


class DiscreteNumericalSaver(ConfigSaver):
    def _serialize(self, config) -> dict:

        d = super()._serialize(config)
        # line symbology
        d |= serialize_pen_param(config, "pen_params", "line")

        # point symbology
        d["point_symbol"] = config.symbol_parameter.value()
        d["point_size"] = config.symbol_size_parameter.value()
        qcolor = config.symbol_color_parameter.value()
        d["point_color"] = qcolor.name()

        # color ramp
        d["color_ramp_name"] = config.color_ramp_name_param.value()
        colormap = config.color_ramp_parameter.value()
        pos, colors = colormap.getStops(mode=pg.ColorMap.BYTE)
        d["color_ramp_scale"] = {"pos": pos.tolist(), "color": colors.tolist()}
        d["color_ramp_min"] = config.min_color_ramp_param.value()
        d["color_ramp_max"] = config.max_color_ramp_param.value()
        d["color_ramp_enable"] = config.color_ramp_group.value()

        # extended config if exists
        d["extended_config"] = None
        if config.extended_config is not None:
            d["extended_config"] = config.extended_config.saverClass()._serialize(
                config.extended_config
            )

        return d

    def _deserialize(self, config, dict: dict) -> None:

        deserialize_pen_param(config, dict, "pen_params", "line")
        # apply changes
        config._pen_updated()

        # points
        config.symbol_parameter.setValue(dict.get("point_symbol"))
        config.symbol_size_parameter.setValue(dict.get("point_size"))
        qcolor = QColor()
        qcolor.setNamedColor(dict.get("point_color"))
        config.symbol_color_parameter.setValue(qcolor)

        # color ramp
        config.color_ramp_name_param.setValue(dict.get("color_ramp_name"))
        scale = dict.get("color_ramp_scale")
        cm = pg.ColorMap(pos=scale.get("pos"), color=scale.get("color"))
        config.color_ramp_parameter.setValue(cm)
        config.min_color_ramp_param.setValue(dict.get("color_ramp_min"))
        config.max_color_ramp_param.setValue(dict.get("color_ramp_max"))
        config.color_ramp_group.setValue(dict.get("color_ramp_enable"))

        # extended config if exists
        if (
            config.extended_config is not None
            and dict.get("extended_config") is not None
        ):
            config.extended_config.saverClass()._deserialize(
                config.extended_config, dict.get("extended_config")
            )


class ExtendedNumericalSaver(ConfigSaver):
    def _serialize(self, config) -> dict:

        d = super()._serialize(config)
        # bar pen
        d |= serialize_pen_param(config, "pen_params", "bar_pen")
        # bar fill
        d["bar_fill_color"] = config.bar_color_param.value().name()
        # color ramp
        d["color_ramp_name"] = config.color_ramp_name_param.value()
        colormap = config.color_ramp_parameter.value()
        pos, colors = colormap.getStops(mode=pg.ColorMap.BYTE)
        d["color_ramp_scale"] = {"pos": pos.tolist(), "color": colors.tolist()}
        d["color_ramp_min"] = config.min_color_ramp_param.value()
        d["color_ramp_max"] = config.max_color_ramp_param.value()
        d["color_ramp_enable"] = config.color_ramp_group.value()

        # discrete config if exists
        d["discrete_config"] = None
        if config.discrete_configuration is not None:
            d[
                "discrete_config"
            ] = config.discrete_configuration.saverClass()._serialize(
                config.discrete_configuration
            )

        return d

    def _deserialize(self, config, dict: dict) -> None:

        deserialize_pen_param(config, dict, "pen_params", "bar_pen")
        # apply changes
        config._pen_updated()
        # bar fill
        qcolor = QColor()
        qcolor.setNamedColor(dict.get("bar_fill_color"))
        config.bar_color_param.setValue(qcolor)
        # color ramp
        config.color_ramp_name_param.setValue(dict.get("color_ramp_name"))
        scale = dict.get("color_ramp_scale")
        cm = pg.ColorMap(pos=scale.get("pos"), color=scale.get("color"))
        config.color_ramp_parameter.setValue(cm)
        if (
            dict.get("color_ramp_min") is not None
            and dict.get("color_ramp_max") is not None
        ):
            config.min_color_ramp_param.setValue(dict.get("color_ramp_min"))
            config.max_color_ramp_param.setValue(dict.get("color_ramp_max"))
        config.color_ramp_group.setValue(dict.get("color_ramp_enable"))

        # discrete config if exists
        if (
            config.discrete_configuration is not None
            and dict.get("discrete_config") is not None
        ):
            config.discrete_configuration.saverClass()._deserialize(
                config.discrete_configuration, dict.get("discrete_config")
            )
