from qgis.PyQt.QtWidgets import QWizard


class EmptyAssayAdminWizard(QWizard):
    """
    Empty wizard. Will be unaccessible since button will be disabled.
    """

    def __init__(self, openlog_connection, openlog_plugin, parent=None) -> None:
        super().__init__(parent)
