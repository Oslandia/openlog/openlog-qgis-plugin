from openlog.plugins.extensions.azimuth_sorting.classes import (
    AssaySorter,
    AzimuthAnnotation,
)
from openlog.plugins.hookspecs import AzimuthSortingHook, hookimpl

PLUGIN_NAME = "azimuth_sorting"


@hookimpl
def azimuth_sorting_plugin():
    return AzimuthSortingHook(
        name=PLUGIN_NAME, enable=False, sorter=AssaySorter, annoter=AzimuthAnnotation
    )
