from qgis.core import (
    QgsGeometryGeneratorSymbolLayer,
    QgsRuleBasedLabeling,
    QgsVectorLayer,
)


class InspectorProjector:
    """
    Class for projecting inspector position and label upon traces.
    Must be implemented in premium extension.
    """

    @staticmethod
    def set_inspector_expression(
        layer,
        hole_ids: list[str] = [],
        length: list[float] = [],
        eoh: list[float] = [],
        colors: list[tuple] = [],
        texts: list[str] = [],
        mode: str = None,
        reset: bool = False,
    ):
        return

    @staticmethod
    def _reset_inspector_layers(layer: QgsVectorLayer):
        return

    @staticmethod
    def _refresh_layers(layer: QgsVectorLayer):

        return

    @staticmethod
    def set_one_inspector_expression(
        layer: QgsVectorLayer, expression: str, color: tuple, text: str
    ):

        return

    @staticmethod
    def _get_empty_inspector_layer(color: tuple) -> QgsGeometryGeneratorSymbolLayer:

        return

    @staticmethod
    def _get_empty_labeling_rule(
        layer_id: str, color: tuple
    ) -> QgsRuleBasedLabeling.Rule:
        return

    @staticmethod
    def add_inspector_layer(layer: QgsVectorLayer, color: tuple):
        return


class InspectorHandler:
    """
    Class for synchronizing inspectors with QGIS layers.
    Must be instantiated as an AssayVisualizationConfigList attribute.
    Must be implemented in premium extension.
    """

    def __init__(self) -> None:
        return

    def set_eohs(self, collar_list):

        return

    def _update_palette(self, inspector_list: list):
        return

    def _assign_color(self, inspector):
        return

    def _is_layer_exist(self, layer):
        return

    def _update_layers(self):
        return

    def _set_expression(self, layer):
        return

    def _merge_inspectors(
        self,
        hole_ids: list,
        lengths: list,
        eohs: list,
        colors: list,
        texts: list,
        columns: list,
    ):
        return

    def _hidden_inspector(self, hidden: bool):
        return

    def synchronize(self):
        return

    def refresh(self, configs: list, layers: list):
        return
