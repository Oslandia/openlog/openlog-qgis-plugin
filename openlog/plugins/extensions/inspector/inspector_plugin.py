# plugin specs
# plugin implementation
from openlog.plugins.extensions.inspector.classes import InspectorHandler
from openlog.plugins.hookspecs import InspectorPluginHook, hookimpl

#: Name of the plugin that will be referenced in our configuration
PLUGIN_NAME = "inspector"


@hookimpl
def inspector_plugin():
    return InspectorPluginHook(
        name=PLUGIN_NAME,
        inspector_handler=InspectorHandler,
        enable=False,
    )
