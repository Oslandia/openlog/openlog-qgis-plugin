from openlog.gui.assay_visualization.config.assay_column_visualization_config import (
    AssayColumnVisualizationConfig,
)


class DetectionLimitHandler:
    """
    Abstract class for handling detection limits visualization.
    Should be initialized as config attribute.
    """

    def __init__(self, config: AssayColumnVisualizationConfig):
        self.config = config

    def display_detection_limits(self):
        return

    def _create_items(self):
        return
