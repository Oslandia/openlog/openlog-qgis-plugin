class DepthTicksHandler:
    def __init__(self, assay_widget):
        self.assay_widget = assay_widget

    def on_depth_ticks_click(self):
        """
        Enable/disable tick layer.
        """

        return

    def on_ticks_interval_change(self):
        """
        Slot to update interval.
        """
        return
