from openlog.plugins.extensions.depth_ticks.classes import DepthTicksHandler
from openlog.plugins.hookspecs import DepthTicksHook, hookimpl

PLUGIN_NAME = "depth_ticks"


@hookimpl
def depth_ticks_plugin():
    return DepthTicksHook(name=PLUGIN_NAME, enable=False, handler=DepthTicksHandler)
