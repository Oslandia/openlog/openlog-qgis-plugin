from openlog.gui.connection.connection_dialog import ConnectionDialog
from openlog.plugins.connections.geotic.mssql_connection_widget import (
    MssqlConnectionWidget,
)
from openlog.toolbelt import PlgTranslator


class GeoticConnectionDialog(ConnectionDialog):
    def __init__(self, parent=None) -> None:
        """
        QDialog to get an geotic connection

        Args:
            parent: QWidget
        """
        super(GeoticConnectionDialog, self).__init__(parent)
        self.set_connection_widget(MssqlConnectionWidget())

        # translation
        self.tr = PlgTranslator().tr
        self.setWindowTitle(self.tr("Geotic connection"))
