from dataclasses import dataclass, field

from openlog.datamodel.assay.generic_assay import SphericalType


@dataclass
class SphericalDefinition:
    dip_col: str = ""
    azimuth_col: str = ""
    polarity_col: str = ""
    type_: str = SphericalType.LINE.name

    def get_spherical_columns(self) -> [str]:
        """
        Get spherical columns

        Returns: [str] spherical columns

        """
        result = []
        result.append(self.dip_col)
        result.append(self.azimuth_col)
        if self.type_ == SphericalType.PLANE:
            result.append(self.polarity_col)

        return result
