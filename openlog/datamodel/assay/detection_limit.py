from dataclasses import dataclass


@dataclass
class AssayColumnDetectionLimit:
    detection_min_col: str = ""
    detection_max_col: str = ""

    def get_detection_columns(self) -> [str]:
        """
        Get detection limit columns

        Returns: [str]

        """
        result = []
        if self.detection_min_col != "" and self.detection_min_col is not None:
            result.append(self.detection_min_col)
        if self.detection_max_col != "" and self.detection_max_col is not None:
            result.append(self.detection_max_col)
        return result

    def is_detection_limits_defined(self) -> bool:
        """
        Is detection limits defined ?
        """
        return self.detection_min_col != "" or self.detection_max_col != ""
