from openlog.datamodel.assay.generic_assay import GenericAssay
from openlog.datamodel.connection.openlog_connection import OpenLogConnection
from openlog.plugins.assay.generic_assay_plugin_column_reader import (
    GenericAssayPluginColumnReader,
)


class AssayFactory:
    """Factory class for assay column visualization creation"""

    def create_assay(
        self,
        openlog_connection: OpenLogConnection,
        hole_id: str,
        assay_name: str,
    ) -> GenericAssay:
        """
        Return GenericAssay for assay data use

        Args:
            openlog_connection (OpenLogConnection): connection to get assay
            hole_id (str): The collar id of the hole
            variable (str): variable name of assay

        Returns: (GenericAssay)

        """
        # Get assay from connection
        assay = openlog_connection.get_assay_iface().get_assay(hole_id, assay_name)

        # If plugin column reader, use specific class that can access plugins
        if assay.use_assay_column_plugin_reader:
            assay = GenericAssayPluginColumnReader(
                hole_id=assay.hole_id,
                assay_definition=assay.assay_definition,
                **assay.kwargs,
            )
        return assay
