from pathlib import Path

from qgis.core import QgsDataSourceUri, QgsVectorLayer

from openlog.datamodel.assay.generic_assay import (
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
)
from openlog.datamodel.connection.interfaces.layers_interface import LayersInterface
from openlog.toolbelt import PlgTranslator


class SpatialiteLayersInterface(LayersInterface):
    def __init__(self, file_path: Path):
        """
        Implements LayersInterface for SpatialiteConnection

        Args:
            file_path: spatialite file path
        """
        super().__init__()
        self._file_path = file_path
        self.tr = PlgTranslator().tr

    def _get_assay_query(self, assay_def: AssayDefinition) -> str:
        """
        Generate SQL queries for assay views.
        """

        if assay_def.domain == AssayDomainType.TIME:
            q = f"SELECT * FROM {assay_def.variable}"
            return q

        column_names = list(assay_def.columns.keys())
        column_sql = ",".join(column_names)

        # TODO : use st_3dlineinterpolatepoint instead, but doesn't work with custom function
        if assay_def.data_extent == AssayDataExtent.DISCRETE:
            q = f"""
                SELECT hole, dataset, person, import_date, x AS depth, c_x as x, c_y as y, c_z as z, {column_sql} FROM
                (SELECT hole,o.*, max(ifnull(eoh, 0), ifnull(planned_eoh, 0)),
                st_x(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS c_x,
                st_y(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS c_y,
                st_z(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS c_z
                FROM {assay_def.variable} AS o
                LEFT JOIN collar AS c
                ON o.hole = c.hole_id) AS tmp
            """

        else:
            q = f"""
                SELECT hole, dataset, person, import_date, x AS top, x_end as bottom,
                f_x as x_top, f_y as y_top, f_z as z_top, t_x as x_bottom, t_y as y_bottom, t_z as z_bottom, {column_sql}
                FROM
                (SELECT hole,o.*, max(ifnull(eoh, 0), ifnull(planned_eoh, 0)),
                st_x(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS f_x,
                st_y(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS f_y,
                st_z(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS f_z,
                st_x(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x_end/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS t_x,
                st_y(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x_end/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS t_y,
                st_z(ATM_transform(ST_Line_Interpolate_Point(ATM_transform(proj_geom_trace, ATM_CreateXRoll(90)), o.x_end/max(ifnull(eoh, 0), ifnull(planned_eoh, 0))), ATM_CreateXRoll(-90))) AS t_z
                FROM {assay_def.variable} AS o
                LEFT JOIN collar AS c
                ON o.hole = c.hole_id) AS tmp
            """

        return q

    def get_assay_layers(
        self, assay_definitions: list[AssayDefinition]
    ) -> list[QgsVectorLayer]:
        """
        Return list of assays as QgsVectorLayers.
        """
        layers = []
        for assay_def in assay_definitions:
            try:
                uri = self._get_datasource_uri()
                query = self._get_assay_query(assay_def)
                uri.setDataSource("", f"({query})", "", "")
                layer_name = self.tr(f"{assay_def.display_name} - [{self._file_path}]")
                layer = QgsVectorLayer(uri.uri(False), layer_name, "spatialite")
                layers.append(layer)
            except:
                continue

        return layers

    def get_collar_layer(self) -> QgsVectorLayer:
        """
        Return collar QgsVectorLayer

        In spatialite collar geometry is available in collar geom column

        """
        if self.collar_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource(
                "",
                "(SELECT * FROM collar LEFT JOIN metadata USING (hole_id))",
                "geom",
                "",
                "",
            )
            self.collar_layer = QgsVectorLayer(
                uri.uri(False), self.get_collar_layer_name(), "spatialite"
            )
            # probably a QGIS bug when layer is empty
            if not self.collar_layer.isValid():
                uri = self._get_datasource_uri()
                uri.setDataSource(
                    "",
                    "collar",
                    "geom",
                    "",
                    "",
                )
                self.collar_layer = QgsVectorLayer(
                    uri.uri(False), self.get_collar_layer_name(), "spatialite"
                )
            self._set_clamping(self.collar_layer)
        return self.collar_layer

    def get_planned_collar_layer(self) -> QgsVectorLayer:
        """
        Return planned_collar QgsVectorLayer

        In spatialite planned collar geometry is available in collar planned_loc column

        """
        if self.planned_collar_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource(
                "",
                "(SELECT * FROM collar LEFT JOIN metadata USING (hole_id))",
                "planned_loc",
                "",
                "",
            )
            self.planned_collar_layer = QgsVectorLayer(
                uri.uri(False), self.get_planned_collar_layer_name(), "spatialite"
            )
            # probably a QGIS bug when layer is empty
            if not self.planned_collar_layer.isValid():
                uri = self._get_datasource_uri()
                uri.setDataSource(
                    "",
                    "collar",
                    "planned_loc",
                    "",
                    "",
                )
                self.planned_collar_layer = QgsVectorLayer(
                    uri.uri(False), self.get_planned_collar_layer_name(), "spatialite"
                )
            self._set_clamping(self.planned_collar_layer)
        return self.planned_collar_layer

    def get_collar_layer_name(self) -> str:
        """
        Get collar layer name

        Returns: (str) collar layer name

        """
        return self.tr("Collar - [{0}]").format(self._file_path)

    def get_planned_collar_layer_name(self) -> str:
        """
        Get planned collar layer name

        Returns: (str) collar layer name

        """
        return self.tr("Planned collar - [{0}]").format(self._file_path)

    def get_collar_trace_layer(self) -> QgsVectorLayer:
        """
        Return collar trace QgsVectorLayer

        In spatialite collar trace geometry is available in collar effective_geom column

        """
        if self.collar_trace_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource("", "(SELECT * FROM collar)", "effective_geom")
            self.collar_trace_layer = QgsVectorLayer(
                uri.uri(False), self.get_collar_trace_layer_name(), "spatialite"
            )
            # probably a QGIS bug when layer is empty
            if not self.collar_trace_layer.isValid():
                uri = self._get_datasource_uri()
                uri.setDataSource(
                    "",
                    "collar",
                    "effective_geom",
                    "",
                    "",
                )
                self.collar_trace_layer = QgsVectorLayer(
                    uri.uri(False), self.get_collar_trace_layer_name(), "spatialite"
                )
            self._set_clamping(self.collar_trace_layer)
        return self.collar_trace_layer

    def get_planned_trace_layer(self) -> QgsVectorLayer:
        """
        Return plannedf trace QgsVectorLayer

        In spatialite collar trace geometry is available in collar planned_geom column

        """
        if self.planned_trace_layer is None:
            uri = self._get_datasource_uri()
            uri.setDataSource("", "(SELECT * FROM collar)", "planned_geom")
            self.planned_trace_layer = QgsVectorLayer(
                uri.uri(False), self.get_planned_trace_layer_name(), "spatialite"
            )
            # probably a QGIS bug when layer is empty
            if not self.planned_trace_layer.isValid():
                uri = self._get_datasource_uri()
                uri.setDataSource(
                    "",
                    "collar",
                    "planned_geom",
                    "",
                    "",
                )
                self.planned_trace_layer = QgsVectorLayer(
                    uri.uri(False), self.get_planned_trace_layer_name(), "spatialite"
                )

            self._set_clamping(self.planned_trace_layer)
        return self.planned_trace_layer

    def splitted_trace_layer_available(self) -> bool:
        """Define if splitted trace layer can be used

        :return: True is splitted trace layer can be used, False otherwise
        :rtype: bool
        """
        return True

    def get_splitted_trace_layer(
        self, column_config, planned: bool = False
    ) -> QgsVectorLayer:
        """
        Return splitted trace QgsVectorLayer

        In spatialite collar trace geometry is available in collar geom_trace column

        """
        layer_name = self.get_splitted_trace_layer_name(column_config, planned)
        id_name = f"{column_config.assay_name}_{column_config.column.name}"
        suffix = "planned_trace" if planned else "trace"

        uri = self._get_datasource_uri()
        uri.setDataSource("", f"{id_name}_{suffix}", "geom_interval")

        splitted_trace_layer = QgsVectorLayer(uri.uri(False), layer_name, "spatialite")
        # check if layer need updates: if so, make it unvalid
        valid = True
        field_names = splitted_trace_layer.fields().names()
        for ftr in splitted_trace_layer.getFeatures():
            attr = ftr.attributes()
            if len(attr) > 0 and attr[field_names.index("need_update")] == 1:
                valid = False
                break
        if valid is False:
            uri = self._get_datasource_uri()
            uri.setDataSource("", f"{id_name}_{suffix}", "fake_column")
            splitted_trace_layer = QgsVectorLayer(
                uri.uri(False), layer_name, "spatialite"
            )

        self._set_clamping(splitted_trace_layer)

        # add to list
        self.splitted_trace_layer.append(splitted_trace_layer)
        self.splitted_trace_layer = [
            lyr
            for lyr in self.splitted_trace_layer
            if self._is_layer_exist(lyr) and lyr.isValid()
        ]

        return splitted_trace_layer

    def get_collar_trace_layer_name(self) -> str:
        return self.tr("Trace - [{0}]").format(self._file_path)

    def get_planned_trace_layer_name(self) -> str:
        return self.tr("Planned trace - [{0}]").format(self._file_path)

    def _get_datasource_uri(self) -> QgsDataSourceUri:
        """
        Get a QgsDataSourceUri from spatialite file

        Returns: QgsDataSourceUri with current connection parameters

        """
        uri = QgsDataSourceUri()
        uri.setDatabase(str(self._file_path))
        return uri
