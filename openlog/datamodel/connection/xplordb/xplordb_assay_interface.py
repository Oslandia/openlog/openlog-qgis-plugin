import sqlalchemy
from sqlalchemy import DDL, event
from xplordb.sqlalchemy.assay.structural_types import pgAzimuthType, pgSphericalType

from openlog.core.geo_extractor import XplordbGeoExtractor
from openlog.core.trace_splitter import XplordbSplitTracesQueries
from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDatabaseDefinition,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
)
from openlog.datamodel.connection.interfaces.categories_interface import (
    CategoriesInterface,
)
from openlog.datamodel.connection.sqlalchemy.sqlalchemy_assay_interface import (
    SqlAlchemyAssayInterface,
)


class XplordbAssayInterface(SqlAlchemyAssayInterface):
    def __init__(self, engine, session, categories_iface: CategoriesInterface):
        """
        Implement AssayInterface for a Xplordb db

        Args:
            engine: sqlalchemy engine
            session: sqlalchemy session created from engine
            categories_iface : CategoriesInterface to get default lith category name
        """
        self._categories_iface = categories_iface
        super().__init__(engine, session, "assay")
        self.trace_splitter = XplordbSplitTracesQueries
        self.geo_extractor = XplordbGeoExtractor

    def default_assay_schema(self) -> str:
        """
        Return default schema for assay table creation

        """
        return "assay"

    def _cast_column_type(
        self, assay_table: sqlalchemy.Table, assay_definition: AssayDefinition
    ) -> sqlalchemy.Table:
        """
        Cast sqlalchemy.Table's columns depending backend engine.
        """
        # define generic function
        def _cast_one_column_type_(
            assay_table: sqlalchemy.Table,
            assay_definition: AssayDefinition,
            source_type: AssaySeriesType,
            target_type: sqlalchemy.types,
        ):
            # some columns have a dedicated postgresql type
            to_remove = [
                n
                for n, v in assay_definition.columns.items()
                if v.series_type == source_type
            ]
            col_instances = [
                col for col in assay_table._columns if col.name in to_remove
            ]
            for col in col_instances:
                assay_table._columns.remove(col)

            # create new columns with specific type
            for col_name in to_remove:
                c = sqlalchemy.Column(col_name, target_type(), primary_key=False)
                assay_table.append_column(c)

            return assay_table

        # spherical : composite type assay.spherical_data
        assay_table = _cast_one_column_type_(
            assay_table, assay_definition, AssaySeriesType.SPHERICAL, pgSphericalType
        )
        # polar : assay.azimuth type
        assay_table = _cast_one_column_type_(
            assay_table, assay_definition, AssaySeriesType.POLAR, pgAzimuthType
        )

        return assay_table

    def _convert_composite_string(
        self, params: list, assay_definition: AssayDefinition
    ) -> list:
        """
        Convert string values to composite type depending backend engine.
        """

        # spherical type has to be converted
        col_names = [
            n
            for n, v in assay_definition.columns.items()
            if v.series_type == AssaySeriesType.SPHERICAL
        ]

        for p in params:
            for name in col_names:
                p[name] = pgSphericalType.insert_from_json(p[name])

        return params

    def _before_sqlalchemy_table_creation(
        self, table: sqlalchemy.Table, schema: str
    ) -> None:
        """
        Define sqlalchemy events before table creation. Used to define specific permission on created table.

        Args:
            base: sqlalchemy base to be created
            schema: (str) database schema used
        """
        schema_str = ""
        if schema:
            schema_str = schema + "."

        event.listen(
            table,
            "after_create",
            DDL(
                f"ALTER TABLE {schema_str}{table.name} OWNER TO xdb_admin;"
                f"GRANT SELECT, TRIGGER ON TABLE {schema_str}{table.name} TO xdb_viewer;"
                f"GRANT SELECT, UPDATE, INSERT, TRIGGER ON TABLE {schema_str}{table.name} TO xdb_logger;"
                f"GRANT ALL ON TABLE {schema_str}{table.name} TO xdb_admin;"
            ),
        )

    def delete_assay_from_database(self, variable: str, only_splitted: bool = False):

        # get splitted layers
        q = f"SELECT name FROM assay.assay_column WHERE assay = '{variable}' AND series_type IN ('numerical', 'categorical');"
        splittable_columns = self.session.execute(q).fetchall()
        splittable_columns = [elt[0] for elt in splittable_columns]

        splitted_layers = []
        trigger_names = []
        table_names = []
        for column in splittable_columns:
            for geom in ["trace", "planned_trace"]:
                l = "_".join([variable, column, geom])
                splitted_layers.append(l)
                for op in ["update", "insert", "delete"]:
                    for table in ["collar", "assay"]:
                        s = "_".join([op, table, variable, column, geom])
                        trigger_names.append(s)
                        # table name
                        table_name = (
                            "display.display_collar"
                            if table == "collar"
                            else f"assay.{variable}"
                        )
                        table_names.append(table_name)

        # delete triggers
        for trigger, table_name in zip(trigger_names, table_names):
            q = f'DROP TRIGGER IF EXISTS "{trigger}" ON {table_name};'
            self.session.execute(q)
            # in lowercase
            q = f'DROP TRIGGER IF EXISTS "{trigger.lower()}" ON {table_name};'
            self.session.execute(q)
            self.session.commit()

        # delete trace layers
        for layer in splitted_layers:
            q = f'DROP TABLE IF EXISTS display."{layer}";'
            self.session.execute(q)
            self.session.commit()

        if only_splitted:
            return

        # delete assay table
        q = f"DROP TABLE IF EXISTS assay.{variable};"
        self.session.execute(q)
        self.session.commit()
        # delete assay definitions
        q = f"DELETE FROM assay.assay_column WHERE assay = '{variable}';"
        self.session.execute(q)
        self.session.commit()

        q = f"DELETE FROM assay.assay_column_definition WHERE variable = '{variable}';"
        self.session.execute(q)
        self.session.commit()

        q = f"DELETE FROM assay.assay_definition WHERE table_name = '{variable}';"
        self.session.execute(q)
        self.session.commit()

        q = f"DELETE FROM assay.assays WHERE variable = '{variable}';"
        self.session.execute(q)
        self.session.commit()

    def can_administrate_assays(self) -> dict:

        result = {"creation": False, "deletion": False}

        try:

            # get user name
            q = "SELECT session_user;"
            user = self.session.execute(q).fetchall()[0][0]

            # retrieve parent role
            q = f"""
                SELECT
                    r_parent.rolname AS parent_role,
                    r_child.rolsuper
                FROM
                    pg_roles r_child
                JOIN
                    pg_auth_members m ON r_child.oid = m.member
                JOIN
                    pg_roles r_parent ON r_parent.oid = m.roleid
                WHERE
                    r_child.rolname = '{user}';

            """

            role_parent, is_superuser = self.session.execute(q).fetchall()[0]

            if is_superuser:
                result["creation"] = True
                result["deletion"] = True
            elif role_parent == "xdb_admin":
                result["creation"] = True
                result["deletion"] = True

            return result
        except:
            return result

    def can_save_symbology_in_db(self) -> bool:
        """
        Check is database can store assay symbology.
        """

        return True
