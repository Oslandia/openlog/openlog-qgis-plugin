from typing import List

from xplordb.datamodel.collar import Collar
from xplordb.datamodel.lith import Lith
from xplordb.datamodel.survey import Survey


class ReadInterface:
    """
    Interface to read data from OpenLogConnection (person, dataset, collar, survey and lith

    Get collar or collar trace layer :

    - :meth:`get_available_person_codes`
    - :meth:`get_available_dataset_names`
    - :meth:`get_collar`
    - :meth:`get_collar_display_name`
    - :meth:`get_surveys_from_collars`

    Lith can be available or not see :

    - :meth:`can_read_liths`
    - :meth:`get_liths_from_collars`

    By default, all functions are not implemented and raises :class:`InvalidInterface` exception.

    """

    class InvalidInterface(Exception):
        pass

    class ReadException(Exception):
        pass

    def get_available_person_codes(self) -> List[str]:
        """
        Return available person codes from connection

        Returns:
            List[str] : raises :class:`InvalidInterface` if not implemented
        """
        raise ReadInterface.InvalidInterface()

    def get_available_dataset_names(self) -> List[str]:
        """
        Return available dataset names from connection

        Returns:
            List[str] : raises :class:`InvalidInterface` if not implemented
        """
        raise ReadInterface.InvalidInterface()

    def get_collar(self, hole_id: str) -> Collar:
        """
        Get collar from hole_id

        Args:
            hole_id:  collar hold_id
        Returns:
            Collar: raises :class:`InvalidInterface` if not implemented
        """
        raise ReadInterface.InvalidInterface()

    def get_all_collars(self) -> list[Collar]:
        """
        The function `get_all_collars` returns a collar object for a given hole_id

        :return: A list object
        """
        raise ReadInterface.InvalidInterface()

    def get_collar_display_name(self, hole_id: str) -> str:
        """
        Get collar display name from hole_id. By default, return hole_id

        Args:
            hole_id: str

        Returns:
            str: hole_id in default interface, hole id is equal to display name
        """
        return hole_id

    def get_surveys_from_collars(self, collars_id: List[str]) -> List[Survey]:
        """
        Return surveys from collar id list

        Args:
            collars_id: list of collar id
        Returns:
            List[Survey]: list of survey for collar list. raises :class:`InvalidInterface` if not implemented
        """
        raise ReadInterface.InvalidInterface()
