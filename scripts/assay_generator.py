from pathlib import Path

import numpy as np


def create_file(tmp_path: Path, filename: str, columns: [str], values: [[str]], delimiter: str) -> Path:
    csv_file = tmp_path / filename
    with csv_file.open('w') as fp:
        fp.write(delimiter.join(columns) + "\n")
        for row in values:
            fp.write(delimiter.join(row) + "\n")
    return csv_file


if __name__ == '__main__':
    # For now only discrete depth assay can be generated : update columns for extended
    columns = ["Hole ID", "Depth"]

    # Add any column
    assay_columns = ["Value", "Value 2"]
    full_columns = columns + assay_columns

    nb_collar = 10
    size = 10000
    values = []
    for i in range(1, nb_collar):
        hole_id = np.full(size, f"DDH0{i}")
        x = np.linspace(0, 200, size)
        y_val = {}
        for col in assay_columns:
            # Noise value
            y_val[col] = np.random.normal(size=size)

        for j in range(0, size):
            val = [str(hole_id[j]), str(x[j])]

            for col in assay_columns:
                val.append(str(y_val[col][j]))

            values.append(val)

    csv_file = create_file(Path("../data/test_data/csv_DH"),
                           'assay.csv',
                           full_columns,
                           values, ',')
